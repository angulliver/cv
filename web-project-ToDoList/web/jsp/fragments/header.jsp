<%@ page import="controllers.ControllerConst" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="/jsp/pages/error.jsp" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>">
</head>
<body>
<div class="header">
    <div class="toMain menuElement">
        <c:choose>
            <c:when test="${empty sessionScope.user}">
                <a href="<c:url value="/index.jsp"/>">To main</a>
            </c:when>
            <c:when test="${not empty sessionScope.user}">
                <a href="<c:url value="/controllers/tasks/ShowTasksController"/>">To main</a>
            </c:when>
        </c:choose>
    </div>
    <div class="currentUser">
        User:
        <c:choose>
            <c:when test="${empty sessionScope.user}">
                <c:out value="guest"/>
            </c:when>
            <c:when test="${not empty sessionScope.user}">
                <c:out value="${sessionScope.user}"/>
            </c:when>
        </c:choose>
    </div>
    <div class="sessionInOut">
        <c:set var="context" value="${pageContext.request.contextPath}"/>
        <c:choose>
            <c:when test="${empty sessionScope.user}">
                <div class="registration menuElement">
                    <a href="${context}<%=ControllerConst.REGISTRATION_PAGE%>">Registration</a>
                </div>
                <div class="login menuElement">
                    <a href="${context}<%=ControllerConst.LOGIN_PAGE%>">Login</a>
                </div>
            </c:when>
            <c:when test="${not empty sessionScope.user}">
                <div class="logout menuElement">
                    <a href="${context}<%=ControllerConst.LOGOUT_CONTROLLER%>">Logout</a>
                </div>
            </c:when>
        </c:choose>
    </div>
</div>
</body>
</html>
