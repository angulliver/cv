<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="/jsp/pages/error.jsp" %>


<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>">
</head>
<body>
<div class="footer">
    <p align="center">Developed by Vinnik Artsiom</p>
</div>
</body>
</html>
