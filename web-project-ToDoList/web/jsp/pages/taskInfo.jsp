<%@ page import="controllers.ControllerConst" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.enums.TaskStatus" %>
<%@ page errorPage="/jsp/pages/error.jsp" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<c:url value="/css/style.css"/>">
    <title>Task information</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/jsp/fragments/header.jsp"/>
    <div class="body">
        <div class="taskTitle single-task-block">
            Title : ${task.title}
        </div>
        <div class="taskDate single-task-block">
            Date : ${task.date}
        </div>
        <div class="clear"></div>
        <div class="taskDescription single-task-block">
            ${task.description}
        </div>
        <c:if test="${task.getFile() ne null}">
            <div class="taskDescription single-task-block">
                File name : ${task.file.getFilename()}
            </div>
        </c:if>

        <c:choose>
            <c:when test="${task.getStatus() == TaskStatus.ACTUAL}">
                <c:if test="${task.getFile() ne null}">
                    <div>
                        <form action="${pageContext.request.contextPath}<%=ControllerConst.DOWNLOAD_FILE_CONTROLLER%>?taskId=${task.id}"
                              method="post">
                            <input type="submit" value="Download file">
                        </form>
                    </div>
                    <div>
                        <form action="${pageContext.request.contextPath}<%=ControllerConst.DELETE_FILE_CONTROLLER%>?taskId=${task.id}"
                              method="post">
                            <input type="submit" value="Delete file">
                        </form>
                    </div>
                </c:if>

                <div>
                    <form action="${pageContext.request.contextPath}<%=ControllerConst.UPLOAD_FILE_CONTROLLER%>?taskId=${task.id}"
                          method="post" enctype="multipart/form-data">
                        <input type="file" name="taskFile" value="Upload file"/>
                        <input type="submit" value="Upload file"/>
                    </form>
                </div>

                <div>
                    <form action="${pageContext.request.contextPath}<%=ControllerConst.CHANGE_TASK_CONTROLLER%>?taskId=${task.id}"
                          method="post">
                        <input type="hidden" name="taskStatus" value="fixed">
                        <input type="submit" value="Mark as fixed">
                    </form>
                </div>

                <div>
                    <form action="${pageContext.request.contextPath}<%=ControllerConst.CHANGE_TASK_CONTROLLER%>?taskId=${task.id}"
                          method="post">
                        <input type="hidden" name="taskStatus" value="deleted">
                        <input type="submit" value="Delete task">
                    </form>
                </div>
            </c:when>

            <c:when test="${task.getStatus() == TaskStatus.FIXED}">
                <c:if test="${task.getFile() ne null}">
                    <div>
                        <form action="${pageContext.request.contextPath}<%=ControllerConst.DOWNLOAD_FILE_CONTROLLER%>?taskId=${task.id}"
                              method="post">
                            <input type="submit" value="Download file">
                        </form>
                    </div>
                    <div>
                        <form action="${pageContext.request.contextPath}<%=ControllerConst.DELETE_FILE_CONTROLLER%>?taskId=${task.id}"
                              method="post">
                            <input type="submit" value="Delete file">
                        </form>
                    </div>
                </c:if>

                <div>
                    <form action="${pageContext.request.contextPath}<%=ControllerConst.CHANGE_TASK_CONTROLLER%>?taskId=${task.id}"
                          method="post">
                        <input type="hidden" name="taskStatus" value="deleted">
                        <input type="submit" value="Delete task">
                    </form>
                </div>
            </c:when>

            <c:when test="${task.getStatus() == TaskStatus.DELETED}">
                <div>
                    <form action="${pageContext.request.contextPath}<%=ControllerConst.DELETE_TASK_CONTROLLER%>?taskId=${task.id}"
                          method="post">
                        <input type="hidden" name="taskOperation" value="clear">
                        <input type="hidden" name="taskId" value="${task.id}">
                        <input type="submit" value="Remove task">
                    </form>
                </div>

                <div>
                    <form action="${pageContext.request.contextPath}<%=ControllerConst.CHANGE_TASK_CONTROLLER%>?taskId=${task.id}"
                          method="post">
                        <input type="hidden" name="taskStatus" value="actual">
                        <input type="submit" value="Restore task">
                    </form>
                </div>
            </c:when>
        </c:choose>

    </div>
    <c:import url="/jsp/fragments/footer.jsp"/>
</div>
</body>
</html>
