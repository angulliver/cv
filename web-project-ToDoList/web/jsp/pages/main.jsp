<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="controllers.ControllerConst" %>
<%@ page import="model.enums.TaskStatus" %>
<%@ page errorPage="/jsp/pages/error.jsp" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ToDo</title>
    <link rel="stylesheet" href="<c:url value="/css/style.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/jsp/fragments/header.jsp"/>
    <div class="body">
        <div class="tasksContainer">

            <div class="board">
                <div class="todoHeader">
                    Today
                </div>
                <jsp:useBean id="now" class="java.util.Date"/>
                <c:set var="todayDate"><fmt:formatDate pattern="yyyy-MM-dd" value="${now}"/></c:set>
                <div class="tasks-block">
                    <c:forEach items="${tasks}" var="task">
                        <c:if test="${task.getStatus() == TaskStatus.ACTUAL and (task.date == todayDate or task.date < todayDate)}">
                            <a href="<c:url value="/controllers/tasks/ShowTaskInfoController?taskId=${task.id}"/>"
                               class="task-link">
                                <div class="task-block">
                                    <div class="task-title">${task.title}</div>
                                    <div class="task-date">
                                        <fmt:formatDate value="${task.date}" pattern="yyyy-MM-dd"/>
                                    </div>
                                </div>
                            </a>
                        </c:if>
                    </c:forEach>
                </div>
                <div class="new-task"><a href="<c:url value="/jsp/pages/createTask.jsp?section=today" />">Add new
                    task...</a></div>
            </div>

            <div class="board">
                <div class="todoHeader">
                    Tomorrow
                </div>
                <jsp:setProperty name="now" property="time" value="${now.time + 86400000}"/>
                <fmt:formatDate var="tomorrowDate" value="${now}" pattern="yyyy-MM-dd"/>
                <c:forEach items="${tasks}" var="task">
                    <c:if test="${task.getStatus() == TaskStatus.ACTUAL and task.date == tomorrowDate}">
                        <a href="<c:url value="/controllers/tasks/ShowTaskInfoController?taskId=${task.id}"/>"
                           class="task-link">
                            <div class="task-block">
                                <div class="task-title">${task.title}</div>
                                <div class="task-date">
                                    <fmt:formatDate value="${task.date}" pattern="yyyy-MM-dd"/>
                                </div>
                            </div>
                        </a>
                    </c:if>
                </c:forEach>
                <div class="new-task"><a href="<c:url value="/jsp/pages/createTask.jsp?section=tomorrow"/>">Add new
                    task...</a></div>
            </div>

            <div class="board">
                <div class="todoHeader">
                    Some days
                </div>
                <c:forEach items="${tasks}" var="task">
                    <c:if test="${task.getStatus() == TaskStatus.ACTUAL and task.date > tomorrowDate}">
                        <a href="<c:url value="/controllers/tasks/ShowTaskInfoController?taskId=${task.id}"/>"
                           class="task-link">
                            <div class="task-block">
                                <div class="task-title">${task.title}</div>
                                <div class="task-date">
                                    <fmt:formatDate value="${task.date}" pattern="yyyy-MM-dd"/>
                                </div>
                            </div>
                        </a>
                    </c:if>
                </c:forEach>
                <div class="new-task"><a href="<c:url value="/jsp/pages/createTask.jsp?section=someday"/>">Add new
                    task...</a></div>
            </div>

            <div class="board">
                <div class="todoHeader">
                    Fixed
                </div>
                <c:forEach items="${tasks}" var="task">
                    <c:if test="${task.getStatus() == TaskStatus.FIXED}">
                        <a href="<c:url value="/controllers/tasks/ShowTaskInfoController?taskId=${task.id}"/>"
                           class="task-link">
                            <div class="task-block">
                                <div class="task-title">${task.title}</div>
                                <div class="task-date">
                                    <fmt:formatDate value="${task.date}" pattern="yyyy-MM-dd"/>
                                </div>
                            </div>
                        </a>
                    </c:if>
                </c:forEach>
            </div>

            <div class="board">
                <div class="todoHeader">
                    Recycle bin
                </div>
                <c:forEach items="${tasks}" var="task">
                    <c:if test="${task.getStatus() == TaskStatus.DELETED}">
                        <a href="<c:url value="/controllers/tasks/ShowTaskInfoController?taskId=${task.id}"/>"
                           class="task-link">
                            <div class="task-block">
                                <div class="task-title">${task.title}</div>
                                <div class="task-date">
                                    <fmt:formatDate value="${task.date}" pattern="yyyy-MM-dd"/>
                                </div>
                            </div>
                        </a>
                    </c:if>
                </c:forEach>
                <div>
                    <form action="${pageContext.request.contextPath}<%=ControllerConst.DELETE_TASK_CONTROLLER%>"
                          method="post">
                        <input class="button" type="hidden" name="taskOperation" value="clearAll">
                        <input type="hidden" name="taskId" value="0">
                        <input type="submit" value="Clear all">
                    </form>
                </div>
            </div>

            <div class="clear"></div>
        </div>
    </div>
</div>
<c:import url="/jsp/fragments/footer.jsp"/>
</div>
</body>
</html>