<%@ page import="controllers.ControllerConst" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isErrorPage="true" %>
<html>
<head>
    <title>Error page</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>">
</head>
<body>
<p>Error page</p>
<c:choose>
    <c:when test="${not empty errorMessage}">
        <h2 class="errorMessage"><c:out value="${errorMessage}"/></h2>
    </c:when>
    <c:when test="${not empty pageContext.errorData.requestURI}">
        <h2>Page requested ${pageContext.errorData.requestURI}</h2>
    </c:when>
    <c:otherwise>
        Unexpected application error
    </c:otherwise>
</c:choose>

<a href="<c:url value="/jsp/pages/main.jsp"/>">Go to Main page</a>
</body>
</html>
