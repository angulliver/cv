<%@ page import="controllers.ControllerConst" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="/jsp/pages/error.jsp" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create task</title>
    <link rel="stylesheet" href="<c:url value="/css/style.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/jsp/fragments/header.jsp"/>
    <div class="body">

        <c:if test="${not empty requestScope.errorMessage}">
            <p class="errorMessage"><c:out value="${requestScope.errorMessage}"/></p>
        </c:if>

        <form action="${pageContext.request.contextPath}<%=ControllerConst.CREATE_TASK_CONTROLLER%>" method="post"
              class="userForm" enctype="multipart/form-data">

            <input type="hidden" name="section" value="${param.section}">

            <div class="form-center">
                <div class="submit-block">
                    <p>Task title:</p>
                    <input type="text" name="taskTitle" class="formInput">

                    <jsp:useBean id="date" class="java.util.Date"/>

                    <c:choose>
                        <c:when test="${param.section eq 'today'}">
                            <div>
                                <p>Date:</p>
                                <input type="date" name="taskDate" class="formInput"
                                       value="<fmt:formatDate value="${date}" pattern="yyyy-MM-dd" />" readonly><br/>
                            </div>
                        </c:when>

                        <c:when test="${param.section eq 'tomorrow'}">
                            <div>
                                <p>Date:</p>
                                <jsp:setProperty name="date" property="time" value="${date.time + 86400000}"/>
                                <input type="date" name="taskDate" class="formInput"
                                       value="<fmt:formatDate value="${date}" pattern="yyyy-MM-dd" />" readonly><br/>

                            </div>
                        </c:when>

                        <c:when test="${param.section eq 'someday'}">
                            <div>
                                <p>Date:</p>
                                <jsp:setProperty name="date" property="time" value="${date.time}"/>
                                <input type="date" name="taskDate" class="formInput"
                                       value="<fmt:formatDate value="${date}" pattern="yyyy-MM-dd" />"><br/>

                            </div>
                        </c:when>
                    </c:choose>

                    <p>File:</p>
                    <input type="file" name="taskFile"/>
                </div>

                <div class="submit-block">
                    <p>Description:</p>
                    <textarea name="taskDescription"></textarea>
                </div>
                <div class="clear"></div>

                <input type="submit" value="Create task" class="formInput">
            </div>

        </form>
        <div class="clear"></div>
    </div>
    <c:import url="/jsp/fragments/footer.jsp"/>
</div>
</body>
</html>