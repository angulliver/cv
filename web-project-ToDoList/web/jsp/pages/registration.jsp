<%@ page import="controllers.ControllerConst" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="/jsp/pages/error.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>">
    <title>Registration</title>

</head>
<body>
<div class="wrapper">
    <c:import url="/jsp/fragments/header.jsp"/>
    <div class="body">
        <c:if test="${not empty requestScope.errorMessage}">
            <p class="errorMessage"><c:out value="${requestScope.errorMessage}"/></p>
        </c:if>
        <form class="registrationForm"
              action="${pageContext.request.contextPath}<%=ControllerConst.REGISTRATION_CONTROLLER%>" method="post">
            <p>Login:</p>
            <input type="text" name="login" class="formInput">
            <p>Password:</p>
            <input type="password" name="password" class="formInput"><br/>
            <p>Repeat password:</p>
            <input type="password" name="passwordRepeat" class="formInput"><br/>
            <input type="submit" value="Submit" class="formInput">
        </form>
    </div>
    <c:import url="/jsp/fragments/footer.jsp"/>
</div>
</body>
</html>
