<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="/jsp/pages/error.jsp" %>
<html>
<head>
    <title>ToDo</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="jsp/fragments/header.jsp"/>
    <div class="body">
        Some information about project...
    </div>
    <c:import url="jsp/fragments/footer.jsp"/>
</div>
</body>
</html>
