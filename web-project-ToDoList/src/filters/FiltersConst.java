package filters;

public final class FiltersConst {

    //filters urls
    public static final String LOGIN_FILTER_URL = "/filters/LoginFilter";

    private FiltersConst() {
    }
}
