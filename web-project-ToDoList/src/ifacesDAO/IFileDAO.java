package ifacesDAO;

import model.beans.File;
import model.impl.DaoException;

import java.io.InputStream;

public interface IFileDAO {

    void uploadFile(int idTask, String fileName, InputStream fileContent) throws DaoException;

    File downloadFile(int idTask) throws DaoException;

    void deleteFile(int idTask) throws DaoException;

}
