package ifacesDAO;

import model.beans.Task;
import model.impl.DaoException;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

public interface ITaskDAO {
    List<Task> getTaskLists(String login) throws DaoException;

    void createTask(String login, String taskTitle, String taskDescription, Date taskDate, String fileName, InputStream fileContent) throws DaoException;

    Task getTaskById(int taskId) throws DaoException;

    void changeTask(int idTasks, String taskStatus) throws DaoException;

    void removeTaskById(int id) throws DaoException;

    void removeTaskByStatus(String status) throws DaoException;
}
