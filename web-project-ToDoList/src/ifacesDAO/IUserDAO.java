package ifacesDAO;

import model.beans.User;

public interface IUserDAO {
    User getUser(String login, String password);

    User addUser(String login, String password);
}
