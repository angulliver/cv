package model.factories;

import ifacesDAO.IFileDAO;
import ifacesDAO.ITaskDAO;
import ifacesDAO.IUserDAO;
import model.impl.DaoException;
import model.impl.db.DbFileImpl;
import model.impl.db.DbTaskImpl;
import model.impl.db.DbUserImpl;

import java.util.HashMap;
import java.util.Map;

public class DAOFactory {
    private static final String NO_DAO_INSTANCE_IN_FACTORY_FOR_CLASS = "No DAO instance in factory for class ";
    private static final Map<Class, Object> DB_DAO = new HashMap<>();

    static {
        DB_DAO.put(IUserDAO.class, new DbUserImpl());
        DB_DAO.put(ITaskDAO.class, new DbTaskImpl());
        DB_DAO.put(IFileDAO.class, new DbFileImpl());
    }

    public static <T> T getDAO(Class<T> daoClass) throws DaoException {
        Object dao = DB_DAO.get(daoClass);
        if (dao == null) {
            throw new DaoException(NO_DAO_INSTANCE_IN_FACTORY_FOR_CLASS + daoClass);
        }
        return daoClass.cast(dao);
    }
}