package model.impl.db;

import ifacesDAO.IUserDAO;
import model.beans.User;
import model.connector.DbConnector;
import model.impl.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DbUserImpl implements IUserDAO {

    private final static int LOGIN_INDEX = 1;
    private final static int PASSWORD_INDEX = 2;

    @Override
    public User getUser(String login, String password) throws DaoException {

        Connection connection = null;
        PreparedStatement psGetUser = null;
        ResultSet rsGetUser = null;

        try {
            connection = DbConnector.getInstance().getConnection();

            psGetUser = connection.prepareStatement(DbConst.GET_USER);
            psGetUser.setString(LOGIN_INDEX, login);
            psGetUser.setString(PASSWORD_INDEX, password);
            rsGetUser = psGetUser.executeQuery();
            if (rsGetUser.next()) {
                return new User(login, password);
            } else {
                throw new DaoException("User with such login does not exist.");
            }
        } catch (SQLException e) {
            throw new DaoException("Error while trying to sign in.", e);
        } finally {
            if (connection != null) {
                DbConnector.getInstance().closeResultSets(rsGetUser);
                DbConnector.getInstance().closeStatements(psGetUser);
                DbConnector.getInstance().closeConnection(connection);
            }
        }
    }

    @Override
    public User addUser(String login, String password) throws DaoException {

        Connection connection = null;
        PreparedStatement psCheckLogin = null;
        PreparedStatement psInsertUser = null;

        try {
            connection = DbConnector.getInstance().getConnection();
            psCheckLogin = connection.prepareStatement(DbConst.CHECK_LOGIN);
            psCheckLogin.setString(LOGIN_INDEX, login);
            psInsertUser = connection.prepareStatement(DbConst.ADD_NEW_USER);
            psInsertUser.setString(LOGIN_INDEX, login);
            psInsertUser.setString(PASSWORD_INDEX, password);
            ResultSet resultSet = null;
            synchronized (IUserDAO.class) {
                resultSet = psCheckLogin.executeQuery();
                if (resultSet.next()) {
                    throw new DaoException("User exist");
                }
                psInsertUser.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("Error when requesting registration.", e);
        } finally {
            if (connection != null) {
                DbConnector.getInstance().closeResultSets();
                DbConnector.getInstance().closeStatements(psCheckLogin, psInsertUser);
                DbConnector.getInstance().closeConnection(connection);
            }
        }
        return new User(login, password);
    }
}
