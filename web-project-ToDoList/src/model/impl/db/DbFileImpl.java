package model.impl.db;

import ifacesDAO.IFileDAO;
import model.beans.File;
import model.connector.DbConnector;
import model.impl.DaoException;

import java.io.InputStream;
import java.sql.*;

public class DbFileImpl implements IFileDAO {

    @Override
    public void uploadFile(int taskId, String fileName, InputStream fileContent) throws DaoException {
        final int TASK_ID_INDEX = 1;
        final int FILE_NAME_INDEX = 1;
        final int FILE_CONTENT_INDEX = 2;
        final int FILE_ID_INDEX = 3;

        Connection connection = null;
        PreparedStatement psGetFileIdByTaskId = null;
        PreparedStatement psUploadFile = null;
        ResultSet resultSet = null;

        try {
            connection = DbConnector.getInstance().getConnection();

            psGetFileIdByTaskId = connection.prepareStatement(DbConst.GET_FILE_ID);
            psGetFileIdByTaskId.setInt(TASK_ID_INDEX, taskId);
            resultSet = psGetFileIdByTaskId.executeQuery();
            if (resultSet.next()) {
                int fileId = resultSet.getInt(1);

                psUploadFile = connection.prepareStatement(DbConst.UPLOAD_FILE);
                psUploadFile.setString(FILE_NAME_INDEX, fileName);
                psUploadFile.setBlob(FILE_CONTENT_INDEX, fileContent);
                psUploadFile.setInt(FILE_ID_INDEX, fileId);
                psUploadFile.executeUpdate();
            }

        } catch (SQLException e) {
            throw new DaoException("Error while trying to create task.", e);
        } finally {
            DbConnector.getInstance().closeResultSets(resultSet);
            DbConnector.getInstance().closeStatements(psGetFileIdByTaskId, psUploadFile);
            DbConnector.getInstance().closeConnection(connection);
        }
    }

    @Override
    public File downloadFile(int idTask) throws DaoException {
        final int TASK_ID_INDEX = 1;
        final int FILE_INDEX = 1;

        Connection connection = null;
        PreparedStatement psGetFileIdByTaskId = null;
        PreparedStatement psGetFile = null;
        ResultSet resultSet = null;
        File file = null;

        try {
            connection = DbConnector.getInstance().getConnection();

            psGetFileIdByTaskId = connection.prepareStatement(DbConst.GET_FILE_ID);
            psGetFileIdByTaskId.setInt(TASK_ID_INDEX, idTask);
            resultSet = psGetFileIdByTaskId.executeQuery();
            if (resultSet.next()) {
                int fileId = resultSet.getInt(1);

                psGetFile = connection.prepareStatement(DbConst.GET_FILE);
                psGetFile.setInt(FILE_INDEX, fileId);
                resultSet = psGetFile.executeQuery();
                if (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String fileName = resultSet.getString("fileName");
                    byte[] byteFileContent = resultSet.getBytes("file");

                    if (fileName == null || fileName.equals("")) {
                        return file;
                    }
                    file = new File(fileName, byteFileContent, id);
                }
            }
        } catch (SQLException e) {
            throw new DaoException("Error while trying to get task list.", e);
        } finally {
            DbConnector.getInstance().closeResultSets(resultSet);
            DbConnector.getInstance().closeStatements(psGetFile, psGetFileIdByTaskId);
            DbConnector.getInstance().closeConnection(connection);
        }
        return file;
    }

    @Override
    public void deleteFile(int idTask) throws DaoException {
        final int TASK_ID_INDEX = 1;
        final int FILE_INDEX = 1;

        Connection connection = null;
        PreparedStatement psGetFileIdByTaskId = null;
        PreparedStatement psDeleteFile = null;
        ResultSet resultSet = null;

        try {
            connection = DbConnector.getInstance().getConnection();

            psGetFileIdByTaskId = connection.prepareStatement(DbConst.GET_FILE_ID);
            psGetFileIdByTaskId.setInt(TASK_ID_INDEX, idTask);
            resultSet = psGetFileIdByTaskId.executeQuery();

            if (resultSet.next()) {
                int fileId = resultSet.getInt(1);

                psDeleteFile = connection.prepareStatement(DbConst.DELETE_FILE);
                psDeleteFile.setInt(FILE_INDEX, fileId);
                psDeleteFile.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("Error while trying to get task list.", e);
        } finally {
            DbConnector.getInstance().closeResultSets(resultSet);
            DbConnector.getInstance().closeStatements(psDeleteFile, psGetFileIdByTaskId);
            DbConnector.getInstance().closeConnection(connection);
        }
    }
}
