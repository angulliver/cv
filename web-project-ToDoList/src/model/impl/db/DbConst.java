package model.impl.db;

public final class DbConst {

    //User
    public final static String GET_USER = "SELECT * FROM users WHERE login = ? AND password = ?";
    public final static String CHECK_LOGIN = "SELECT login FROM users WHERE login = ?";
    public final static String ADD_NEW_USER = "INSERT INTO users (login, password) VALUES (?,?)";

    //Tasks
    public static final String GET_USER_ID = "SELECT id FROM users WHERE login = ?";
    public static final String INSERT_TASK = "INSERT INTO tasks (userId, fileId, title, description, date, status) VALUES (?,?,?,?,?,?)";
    public static final String GET_TASK_LIST = "SELECT * FROM tasks WHERE userId = ?";
    public static final String GET_TASK = "SELECT * FROM tasks WHERE id = ?";
    public static final String EDIT_TASK_STATUS = "UPDATE tasks SET status = ? WHERE id = ?";
    public static final String CLEAR_TASK = "DELETE FROM tasks WHERE id = ?";
    public static final String CLEAR_ALL_TASKS = "DELETE FROM tasks WHERE status = 'deleted'";


    //Files
    public static final String UPLOAD_FILE = "UPDATE files SET fileName = ?, file = ? WHERE id = ?";
    public static final String INSERT_FILE = "INSERT INTO files (fileName, file) VALUES (?,?)";
    public static final String GET_FILE = "SELECT * FROM files WHERE id = ?";
    public static final String DELETE_FILE = "UPDATE files SET fileName = NULL, file = NULL WHERE id = ?";
    public static final String GET_FILE_ID = "SELECT fileId FROM tasks WHERE id = ?";

    private DbConst() {
    }
}
