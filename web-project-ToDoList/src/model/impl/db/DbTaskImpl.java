package model.impl.db;

import ifacesDAO.ITaskDAO;
import model.beans.File;
import model.beans.Task;
import model.connector.DbConnector;
import model.enums.TaskStatus;
import model.impl.DaoException;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DbTaskImpl implements ITaskDAO {
    @Override
    public List<Task> getTaskLists(String login) throws DaoException {
        final int LOGIN_ID_INDEX = 1;
        final int ID_INDEX = 1;
        final int TITLE_INDEX = 4;
        final int DESCRIPTION_INDEX = 5;
        final int DATE_INDEX = 6;
        final int STATUS_INDEX = 7;

        List<Task> tasks = new ArrayList<>();
        Connection connection = null;
        PreparedStatement psGetUserId = null;
        PreparedStatement psGetTasks = null;
        ResultSet resultSet = null;

        try {
            connection = DbConnector.getInstance().getConnection();
            psGetUserId = connection.prepareStatement(DbConst.GET_USER_ID);
            psGetUserId.setString(LOGIN_ID_INDEX, login);
            resultSet = psGetUserId.executeQuery();
            if (resultSet.next()) {
                int userId = resultSet.getInt("id");

                psGetTasks = connection.prepareStatement(DbConst.GET_TASK_LIST);
                psGetTasks.setInt(LOGIN_ID_INDEX, userId);
                resultSet = psGetTasks.executeQuery();
                while (resultSet.next()) {

                    int id = resultSet.getInt(ID_INDEX);
                    String title = resultSet.getString(TITLE_INDEX);
                    String description = resultSet.getString(DESCRIPTION_INDEX);
                    Date date = resultSet.getDate(DATE_INDEX);
                    String status = resultSet.getString(STATUS_INDEX);

                    tasks.add(new Task(id, title, description, date, status));
                }
            }
        } catch (SQLException e) {
            throw new DaoException("Error while trying to create task.", e);
        } finally {
            DbConnector.getInstance().closeResultSets(resultSet);
            DbConnector.getInstance().closeStatements(psGetUserId, psGetTasks);
            DbConnector.getInstance().closeConnection(connection);
        }
        return tasks;
    }


    @Override
    public Task getTaskById(int taskId) throws DaoException {
        final int TASK_ID_INDEX = 1;
        final int ID_INDEX = 1;
        final int TITLE_INDEX = 4;
        final int DESCRIPTION_INDEX = 5;
        final int DATE_INDEX = 6;
        final int STATUS_INDEX = 7;

        Task task = null;
        Connection connection = null;
        PreparedStatement psSelectTask = null;
        ResultSet resultSet = null;

        try {
            connection = DbConnector.getInstance().getConnection();

            psSelectTask = connection.prepareStatement(DbConst.GET_TASK);
            psSelectTask.setInt(TASK_ID_INDEX, taskId);
            resultSet = psSelectTask.executeQuery();
            if (resultSet.next()) {

                int id = resultSet.getInt(ID_INDEX);
                DbFileImpl dbFile = new DbFileImpl();
                File file = dbFile.downloadFile(taskId);
                String title = resultSet.getString(TITLE_INDEX);
                Date date = resultSet.getDate(DATE_INDEX);
                String description = resultSet.getString(DESCRIPTION_INDEX);
                String status = resultSet.getString(STATUS_INDEX);

                task = new Task(id, file, title, description, date, status);
            }
        } catch (SQLException e) {
            throw new DaoException("Error while trying to get task list.", e);
        } finally {
            DbConnector.getInstance().closeResultSets(resultSet);
            DbConnector.getInstance().closeStatements(psSelectTask);
            DbConnector.getInstance().closeConnection(connection);
        }
        return task;
    }

    @Override
    public void createTask(String login, String taskTitle, String taskDescription, Date taskDate, String fileName, InputStream fileContent) throws DaoException {
        final int FILE_NAME_INDEX = 1;
        final int FILE_CONTENT_INDEX = 2;

        final int LOGIN_ID_INDEX = 1;
        final int FILE_ID_INDEX = 2;
        final int TITLE_INDEX = 3;
        final int DESCRIPTION_INDEX = 4;
        final int DATE_INDEX = 5;
        final int STATUS_INDEX = 6;

        Connection connection = null;
        PreparedStatement psSelectUserId = null;
        PreparedStatement psInsertTask = null;
        PreparedStatement psUploadFile = null;
        ResultSet resultSet = null;

        int fileId = 0;
        int userId = 0;

        try {
            connection = DbConnector.getInstance().getConnection();

            psUploadFile = connection.prepareStatement(DbConst.INSERT_FILE, Statement.RETURN_GENERATED_KEYS);
            psUploadFile.setString(FILE_NAME_INDEX, fileName);
            psUploadFile.setBlob(FILE_CONTENT_INDEX, fileContent);
            psUploadFile.executeUpdate();
            resultSet = psUploadFile.getGeneratedKeys();
            if (resultSet.next()) {
                fileId = resultSet.getInt(1);
            }

            psSelectUserId = connection.prepareStatement(DbConst.GET_USER_ID);
            psSelectUserId.setString(LOGIN_ID_INDEX, login);
            resultSet = psSelectUserId.executeQuery();
            if (resultSet.next()) {
                userId = resultSet.getInt("id");
            }

            psInsertTask = connection.prepareStatement(DbConst.INSERT_TASK);
            psInsertTask.setInt(LOGIN_ID_INDEX, userId);
            psInsertTask.setInt(FILE_ID_INDEX, fileId);
            psInsertTask.setString(TITLE_INDEX, taskTitle);
            psInsertTask.setString(DESCRIPTION_INDEX, taskDescription);
            psInsertTask.setDate(DATE_INDEX, java.sql.Date.valueOf(taskDate.toString()));
            psInsertTask.setString(STATUS_INDEX, TaskStatus.ACTUAL.toString());
            psInsertTask.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error while trying to create task.", e);
        } finally {
            DbConnector.getInstance().closeResultSets(resultSet);
            DbConnector.getInstance().closeStatements(psSelectUserId, psInsertTask);
            DbConnector.getInstance().closeConnection(connection);
        }
    }

    @Override
    public void changeTask(int idTasks, String taskStatus) throws DaoException {
        final int TASK_STATUS_INDEX = 1;
        final int EDITED_TASK_ID_INDEX = 2;

        Connection connection = null;
        PreparedStatement psEditTaskStatus = null;

        try {
            connection = DbConnector.getInstance().getConnection();

            psEditTaskStatus = connection.prepareStatement(DbConst.EDIT_TASK_STATUS);
            psEditTaskStatus.setString(TASK_STATUS_INDEX, taskStatus);
            psEditTaskStatus.setInt(EDITED_TASK_ID_INDEX, idTasks);
            psEditTaskStatus.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error while trying to change task.", e);
        } finally {
            DbConnector.getInstance().closeStatements(psEditTaskStatus);
            DbConnector.getInstance().closeConnection(connection);
        }
    }

    @Override
    public void removeTaskById(int taskId) throws DaoException {
        final int TASK_ID_INDEX = 1;

        Connection connection = null;
        PreparedStatement psClearTask = null;

        try {
            connection = DbConnector.getInstance().getConnection();

            psClearTask = connection.prepareStatement(DbConst.CLEAR_TASK);
            psClearTask.setInt(TASK_ID_INDEX, taskId);
            psClearTask.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error while trying to clear tasks.", e);
        } finally {
            DbConnector.getInstance().closeStatements(psClearTask);
            DbConnector.getInstance().closeConnection(connection);
        }
    }

    @Override
    public void removeTaskByStatus(String status) throws DaoException {

        Connection connection = null;
        PreparedStatement psClearAllTasks = null;

        try {
            connection = DbConnector.getInstance().getConnection();

            psClearAllTasks = connection.prepareStatement(DbConst.CLEAR_ALL_TASKS);
            psClearAllTasks.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error while trying to create task.", e);
        } finally {
            DbConnector.getInstance().closeStatements(psClearAllTasks);
            DbConnector.getInstance().closeConnection(connection);
        }
    }

}
