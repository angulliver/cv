package model.connector;

import java.sql.*;

public final class DbConnector {

    private final static DbConnector INSTANCE = new DbConnector();

    private String db_url = "jdbc:mysql://localhost:3306/todo?useSSL=false";
    private String db_dr_url = "com.mysql.jdbc.Driver";
    private String db_username = "root";
    private String db_password = "root";

    private DbConnector() {
        try {
            Class.forName(db_dr_url);
        } catch (ClassNotFoundException e) {
            System.err.println("Connection opening problem : " + e);
        }
    }

    public static DbConnector getInstance() {
        return INSTANCE;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(db_url, db_username, db_password);
    }

    public void closeResultSets(ResultSet... resultSets) {
        for (ResultSet resultSet : resultSets) {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    System.err.println("Resource closing problem : " + e);
                }
            }
        }
    }

    public void closeStatements(Statement... statements) {
        for (Statement statement : statements) {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    System.err.println("Resource closing problem : " + e);
                }
            }
        }

    }

    public void closeConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            System.err.println("Resource closing problem : " + e);
        }
    }
}
