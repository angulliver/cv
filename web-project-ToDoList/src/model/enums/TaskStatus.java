package model.enums;

public enum TaskStatus {
    ACTUAL, DELETED, FIXED;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
