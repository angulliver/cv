package model.beans;

public class File {
    private int taskId;
    private String filename;
    private byte[] byteFileContent;

    public File() {
        super();
    }

    public File(String filename, byte[] byteFileContent, int taskId) {
        this.filename = filename;
        this.byteFileContent = byteFileContent;
        this.taskId = taskId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getByteFileContent() {
        return byteFileContent;
    }

    public void setByteFileContent(byte[] byteFileContent) {
        this.byteFileContent = byteFileContent;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return "File{" +
                "taskId=" + taskId +
                ", filename='" + filename + '\'' +
                '}';
    }
}
