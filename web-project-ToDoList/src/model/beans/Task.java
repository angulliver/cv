package model.beans;

import model.beans.File;
import model.enums.TaskStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Task {

    private static final String DATE_PATTERN = "yyyy-MM-dd";

    private final int id;
    private String title;
    private String description;
    private Date date;
    private File file;
    private TaskStatus status;

    public Task() {
        this.id = 0;
    }

    public Task(int id, File file, String title, String description, Date date, String status) {
        this.id = id;
        this.file = file;
        this.title = title;
        this.description = description;
        this.date = date;
        this.status = TaskStatus.valueOf(status.toUpperCase());
    }

    public Task(int id, String title, String description, Date date, String status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.status = TaskStatus.valueOf(status.toUpperCase());
    }

    public Task(String title, String description, Date date) {
        this.id = 0;
        this.title = title;
        this.description = description;
        this.date = date;
        this.status = TaskStatus.ACTUAL;
    }

    private static Date parseStringDate(String date) {
        try {
            return new SimpleDateFormat(DATE_PATTERN).parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public long getTimeDate() {
        return date.getTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                Objects.equals(description, task.description) &&
                Objects.equals(date, task.date) &&
                Objects.equals(file, task.file) &&
                status == task.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, date, file, status);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", status=" + status +
                '}';
    }
}
