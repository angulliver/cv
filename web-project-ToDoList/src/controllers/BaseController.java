package controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class BaseController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        performTask(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        performTask(req, resp);
    }

    protected abstract void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;

    protected void redirect(String url, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(url);
    }

    protected void forward(String url, HttpServletRequest req,
                           HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
        rd.forward(req, resp);
    }

    protected void forwardError(String message, String url, HttpServletRequest req,
                                HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute(ControllerConst.KEY_ERROR_MESSAGE, message);
        forward(url, req, resp);
    }
}
