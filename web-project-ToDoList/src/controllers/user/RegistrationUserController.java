package controllers.user;

import controllers.BaseController;
import controllers.ControllerConst;
import ifacesDAO.IUserDAO;
import model.beans.User;
import model.factories.DAOFactory;
import model.impl.DaoException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;
import java.io.IOException;

@WebServlet(name = "RegistrationUserController", urlPatterns = ControllerConst.REGISTRATION_CONTROLLER)
public class RegistrationUserController extends BaseController {

    private static void checkInput(String login, String password, String passwordRepeat) throws ValidationException {
        if (login == null || password == null) {
            throw new ValidationException("Null line.");
        }
        login = login.trim();
        password = password.trim();
        if ("".equals(login) || "".equals(password)) {
            throw new ValidationException("Empty line.");
        }
        if (!password.equals(passwordRepeat)) {
            throw new ValidationException("Wrong repeat password.");
        }
    }

    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter(ControllerConst.KEY_LOGIN);
        String password = req.getParameter(ControllerConst.KEY_PASSWORD);
        String passwordRepeat = req.getParameter(ControllerConst.KEY_PASSWORD_REPEAT);

        try {
            checkInput(login, password, passwordRepeat);

            IUserDAO userDAO = DAOFactory.getDAO(IUserDAO.class);
            User user = userDAO.addUser(login.trim(), password.trim());
            HttpSession session = req.getSession();
            session.setAttribute(ControllerConst.KEY_USER, user.getLogin());

            forward(ControllerConst.MAIN_PAGE, req, resp);
        } catch (ValidationException | DaoException e) {
            forwardError(e.getMessage(), ControllerConst.REGISTRATION_PAGE, req, resp);
        }
    }
}