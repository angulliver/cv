package controllers.user;

import controllers.BaseController;
import controllers.ControllerConst;
import ifacesDAO.IUserDAO;
import model.beans.User;
import model.factories.DAOFactory;
import model.impl.DaoException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;
import java.io.IOException;

@WebServlet(name = "LoginUserController", urlPatterns = ControllerConst.LOGIN_CONTROLLER)
public class LoginUserController extends BaseController {

    private static void checkInput(String login, String password) throws ValidationException {
        if (login == null || password == null) {
            throw new ValidationException("Null line.");
        }
        login = login.trim();
        password = password.trim();
        if ("".equals(login) || "".equals(password)) {
            throw new ValidationException("Empty line.");
        }
    }

    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter(ControllerConst.KEY_LOGIN);
        String password = req.getParameter(ControllerConst.KEY_PASSWORD);

        try {
            checkInput(login, password);

            IUserDAO userDAO = DAOFactory.getDAO(IUserDAO.class);
            User user = userDAO.getUser(login.trim(), password.trim());
            HttpSession session = req.getSession();
            session.setAttribute(ControllerConst.KEY_USER, user.getLogin());

            forward(ControllerConst.SHOW_TASKS_CONTROLLER, req, resp);
        } catch (ValidationException | DaoException e) {
            forwardError(e.getMessage(), ControllerConst.LOGIN_PAGE, req, resp);
        }
    }
}
