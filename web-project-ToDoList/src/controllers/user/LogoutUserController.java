package controllers.user;

import controllers.BaseController;
import controllers.ControllerConst;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LogoutUserController", urlPatterns = ControllerConst.LOGOUT_CONTROLLER)
public class LogoutUserController extends BaseController {

    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        redirect(req.getContextPath() + ControllerConst.INDEX_PAGE, resp);
    }
}