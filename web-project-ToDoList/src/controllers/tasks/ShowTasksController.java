package controllers.tasks;

import controllers.BaseController;
import controllers.ControllerConst;
import ifacesDAO.ITaskDAO;
import model.beans.Task;
import model.factories.DAOFactory;
import model.impl.DaoException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowTasksController", urlPatterns = ControllerConst.SHOW_TASKS_CONTROLLER)
public class ShowTasksController extends BaseController {

    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding(ControllerConst.CHARACTER_ENCODING);
        String login = (String) req.getSession().getAttribute(ControllerConst.KEY_USER);

        try {
            ITaskDAO taskDAO = DAOFactory.getDAO(ITaskDAO.class);
            List<Task> taskLists = taskDAO.getTaskLists(login);
            req.setAttribute(ControllerConst.KEY_TASK_LIST, taskLists);

            forward(ControllerConst.MAIN_PAGE, req, resp);
        } catch (DaoException e) {
            forwardError(e.getMessage(), ControllerConst.ERROR_PAGE, req, resp);
        }


    }
}
