package controllers.tasks;

import controllers.BaseController;
import controllers.ControllerConst;
import ifacesDAO.ITaskDAO;
import model.factories.DAOFactory;
import model.impl.DaoException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ChangeTaskController", urlPatterns = ControllerConst.CHANGE_TASK_CONTROLLER)
public class ChangeTaskController extends BaseController {


    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int idTaskToChange = Integer.parseInt(req.getParameter(ControllerConst.KEY_TASK_ID));
        String taskStatus = req.getParameter(ControllerConst.KEY_TASK_STATUS);

        try {
            ITaskDAO iTaskDAO = DAOFactory.getDAO(ITaskDAO.class);
            iTaskDAO.changeTask(idTaskToChange, taskStatus);

            redirect(req.getContextPath() + ControllerConst.SHOW_TASKS_CONTROLLER, resp);
        } catch (DaoException e) {
            forwardError(e.getMessage(), ControllerConst.ERROR_PAGE, req, resp);
        }
    }
}
