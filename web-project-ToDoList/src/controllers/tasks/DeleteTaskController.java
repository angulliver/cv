package controllers.tasks;

import controllers.BaseController;
import controllers.ControllerConst;
import ifacesDAO.ITaskDAO;
import model.factories.DAOFactory;
import model.impl.DaoException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DeleteTaskController", urlPatterns = ControllerConst.DELETE_TASK_CONTROLLER)
public class DeleteTaskController extends BaseController {

    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int idTaskToClear = Integer.parseInt(req.getParameter(ControllerConst.KEY_TASK_ID));
        String taskOperation = req.getParameter(ControllerConst.KEY_TASK_OPERATION);

        try {
            if (taskOperation.equals(ControllerConst.KEY_TASK_OPERATION_CLEAR)) {

                ITaskDAO iTaskDAO = DAOFactory.getDAO(ITaskDAO.class);
                iTaskDAO.removeTaskById(idTaskToClear);
                forward(ControllerConst.SHOW_TASKS_CONTROLLER, req, resp);

            } else if (taskOperation.equals(ControllerConst.KEY_TASK_OPERATION_CLEAR_ALL)) {

                ITaskDAO iTaskDAO = DAOFactory.getDAO(ITaskDAO.class);
                iTaskDAO.removeTaskByStatus(ControllerConst.KEY_TASK_STATUS_DELETED);
                forward(ControllerConst.SHOW_TASKS_CONTROLLER, req, resp);
            }
        } catch (DaoException e) {
            forwardError(e.getMessage(), ControllerConst.ERROR_PAGE, req, resp);
        }

    }
}
