package controllers.tasks;

import controllers.BaseController;
import controllers.ControllerConst;
import ifacesDAO.ITaskDAO;
import model.factories.DAOFactory;
import model.impl.DaoException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.bind.ValidationException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.Date;

@WebServlet(name = "CreateTaskController", urlPatterns = ControllerConst.CREATE_TASK_CONTROLLER)
@MultipartConfig(maxFileSize = ControllerConst.MAX_FILE_SIZE)
public class CreateTaskController extends BaseController {

    private static void checkInput(String taskTitle, String taskDescription, Date taskDate) throws ValidationException {
        if (taskTitle == null || taskDescription == null || taskDate == null
                || "".equals(taskTitle) || "".equals(taskDescription)) {
            throw new ValidationException("Incorrect task data.");
        }
    }

    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding(ControllerConst.CHARACTER_ENCODING);

        String login = (String) req.getSession().getAttribute(ControllerConst.KEY_USER);
        String section = (String) req.getParameter(ControllerConst.KEY_TASK_SECTION);
        String taskTitle = req.getParameter(ControllerConst.KEY_TASK_TITLE);
        String taskDescription = req.getParameter(ControllerConst.KEY_TASK_DESCRIPTION);
        Date taskDate = Date.valueOf(req.getParameter(ControllerConst.KEY_TASK_DATE));

        Part filePart = req.getPart(ControllerConst.KEY_TASK_FILE);
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        InputStream fileContent = filePart.getInputStream();

        try {
            checkInput(taskTitle, taskDescription, taskDate);

            ITaskDAO iTaskDAO = DAOFactory.getDAO(ITaskDAO.class);
            iTaskDAO.createTask(login, taskTitle, taskDescription, taskDate, fileName, fileContent);

            redirect(req.getContextPath() + ControllerConst.SHOW_TASKS_CONTROLLER, resp);
        } catch (ValidationException | DaoException e) {
            forwardError(e.getMessage(), ControllerConst.CREATE_TASK_PAGE, req, resp);
        }
    }
}
