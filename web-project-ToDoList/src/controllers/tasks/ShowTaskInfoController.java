package controllers.tasks;

import controllers.BaseController;
import controllers.ControllerConst;
import ifacesDAO.ITaskDAO;
import model.beans.Task;
import model.factories.DAOFactory;
import model.impl.DaoException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ShowTaskInfoController", urlPatterns = ControllerConst.SHOW_TASK_INFO_CONTROLLER)
public class ShowTaskInfoController extends BaseController {

    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding(ControllerConst.CHARACTER_ENCODING);
        int taskId = Integer.parseInt(req.getParameter(ControllerConst.KEY_TASK_ID));

        try {
            ITaskDAO taskDAO = DAOFactory.getDAO(ITaskDAO.class);

            Task task = taskDAO.getTaskById(taskId);
            req.setAttribute(ControllerConst.KEY_TASK, task);

            forward(ControllerConst.TASK_INFO_PAGE, req, resp);
        } catch (DaoException e) {
            forwardError(e.getMessage(), ControllerConst.ERROR_PAGE, req, resp);
        }
    }
}
