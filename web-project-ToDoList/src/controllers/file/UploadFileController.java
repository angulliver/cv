package controllers.file;


import controllers.BaseController;
import controllers.ControllerConst;
import ifacesDAO.IFileDAO;
import model.factories.DAOFactory;
import model.impl.DaoException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

@WebServlet(name = "UploadFileController", urlPatterns = ControllerConst.UPLOAD_FILE_CONTROLLER)
@MultipartConfig(maxFileSize = ControllerConst.MAX_FILE_SIZE)
public class UploadFileController extends BaseController {

    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int taskId = Integer.parseInt(req.getParameter(ControllerConst.KEY_TASK_ID));
        Part filePart = req.getPart(ControllerConst.KEY_TASK_FILE);
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        InputStream fileContent = filePart.getInputStream();

        try {
            IFileDAO fileDAO = DAOFactory.getDAO(IFileDAO.class);
            fileDAO.uploadFile(taskId, fileName, fileContent);

            redirect(req.getContextPath() + ControllerConst.SHOW_TASK_INFO_CONTROLLER + "?taskId=" + taskId, resp);
        } catch (DaoException e) {
            forwardError(e.getMessage(), ControllerConst.ERROR_PAGE, req, resp);
        }
    }
}
