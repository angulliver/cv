package controllers.file;


import controllers.BaseController;
import controllers.ControllerConst;
import ifacesDAO.IFileDAO;
import model.factories.DAOFactory;
import model.impl.DaoException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DeleteFileController", urlPatterns = ControllerConst.DELETE_FILE_CONTROLLER)
public class DeleteFileController extends BaseController {

    @Override
    protected void performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int taskId = Integer.parseInt(req.getParameter(ControllerConst.KEY_TASK_ID));

        try {
            IFileDAO fileDAO = DAOFactory.getDAO(IFileDAO.class);
            fileDAO.deleteFile(taskId);

            redirect(req.getContextPath() + ControllerConst.SHOW_TASK_INFO_CONTROLLER + "?taskId=" + taskId, resp);
        } catch (DaoException e) {
            forwardError(e.getMessage(), ControllerConst.ERROR_PAGE, req, resp);
        }
    }
}
