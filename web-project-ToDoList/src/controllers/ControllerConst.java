package controllers;

public final class ControllerConst {

    //jsp keys
    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_USER = "user";
    public static final String KEY_PASSWORD_REPEAT = "passwordRepeat";
    public static final String KEY_ERROR_MESSAGE = "errorMessage";
    public static final String KEY_TASK_DESCRIPTION = "taskDescription";
    public static final String KEY_TASK_TITLE = "taskTitle";
    public static final String KEY_TASK_DATE = "taskDate";
    public static final String KEY_TASK_STATUS = "taskStatus";
    public static final String KEY_TASK_ID = "taskId";
    public static final String KEY_TASK_LIST = "tasks";
    public static final String KEY_TASK_FILE = "taskFile";
    public static final String KEY_TASK = "task";
    public static final String KEY_TASK_OPERATION = "taskOperation";
    public static final String KEY_TASK_OPERATION_CLEAR = "clear";
    public static final String KEY_TASK_OPERATION_CLEAR_ALL = "clearAll";
    public static final String KEY_TASK_STATUS_DELETED = "deleted";
    public static final String KEY_TASK_SECTION = "section";


    //controllers urls
    public static final String LOGIN_CONTROLLER = "/controllers/user/LoginUserController";
    public static final String LOGOUT_CONTROLLER = "/controllers/user/LogoutUserController";
    public static final String REGISTRATION_CONTROLLER = "/controllers/user/RegistrationUserController";
    public static final String SHOW_TASKS_CONTROLLER = "/controllers/tasks/ShowTasksController";
    public static final String CREATE_TASK_CONTROLLER = "/controllers/tasks/CreateTaskController";
    public static final String CHANGE_TASK_CONTROLLER = "/controllers/tasks/ChangeTaskController";
    public static final String DELETE_TASK_CONTROLLER = "/controllers/tasks/DeleteTaskController";
    public static final String SHOW_TASK_INFO_CONTROLLER = "/controllers/tasks/ShowTaskInfoController";
    public static final String UPLOAD_FILE_CONTROLLER = "/controllers/file/UploadFileController";
    public static final String DELETE_FILE_CONTROLLER = "/controllers/file/DeleteFileController";
    public static final String DOWNLOAD_FILE_CONTROLLER = "/controllers/file/DownloadFileController";

    //pages url
    public static final String INDEX_PAGE = "/index.jsp";
    public static final String MAIN_PAGE = "/jsp/pages/main.jsp";
    public static final String LOGIN_PAGE = "/jsp/pages/login.jsp";
    public static final String REGISTRATION_PAGE = "/jsp/pages/registration.jsp";
    public static final String TASK_INFO_PAGE = "/jsp/pages/taskInfo.jsp";
    public static final String CREATE_TASK_PAGE = "/jsp/pages/createTask.jsp";
    public static final String ERROR_PAGE = "/jsp/pages/error.jsp";

    public static final String CHARACTER_ENCODING = "UTF-8";
    public static final int MAX_FILE_SIZE = 1024 * 1024 * 1000;

    private ControllerConst() {
    }
}
