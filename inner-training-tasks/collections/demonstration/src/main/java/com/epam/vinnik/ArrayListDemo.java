package com.epam.vinnik;

import com.epam.vinnik.collections.list.ArrayList;
import com.epam.vinnik.collections.list.LinkedList;
import com.epam.vinnik.collections.map.EntityImpl;
import com.epam.vinnik.iterator.Iterator;

import java.util.Arrays;

public class ArrayListDemo {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();

        int index = arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);

        Iterator iterator = arrayList.getIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        System.out.println("add: " + index);

        linkedList.add(6);
        linkedList.add(7);
        linkedList.add(4);
        linkedList.add(9);
        linkedList.add(10);

        Integer set = arrayList.set(0, 10);

        System.out.println("set: " + set);

        Integer remove = arrayList.remove(2);

        System.out.println("remove: " + remove);

        int find = arrayList.find(10);

        System.out.println("find: " + find);

        Integer get = arrayList.get(0);

        System.out.println("get: " + get);

        arrayList.add(null);

        arrayList.trim();
        System.out.println("trim: " + Arrays.toString(arrayList.toArray()));

        arrayList.addAll(linkedList);
        System.out.println("add all: " + Arrays.toString(arrayList.toArray()));

        Object[] toArray = arrayList.toArray(linkedList.toArray());
        System.out.println("to array: " + Arrays.toString(toArray));

        arrayList.setMaxSize(5);
        System.out.println("set max size: " + Arrays.toString(arrayList.toArray()));

        System.out.println("get max size: " + arrayList.getMaxSize());

        arrayList.clear();
        System.out.println("clear and size: " + arrayList.size());

        arrayList.clear();

        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);

        arrayList.filterMatches(linkedList);
        System.out.println("filter matches: " + Arrays.toString(arrayList.toArray()));

        arrayList.clear();

        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);

        arrayList.filterDifference(linkedList);
        System.out.println("filter difference: " + Arrays.toString(arrayList.toArray()));


        System.out.println("\n\n\ntest with com.epam.vinnik.User: \n");

        ArrayList<User> arrayListUser = new ArrayList<>();
        LinkedList<User> linkedListUser = new LinkedList<>();
        User user1 = new User("Alex", "adklas@safa.sa", 22, new EntityImpl<>("sex", "male"));
        User user2 = new User("Nik", "adklas@safa.sa", 23, new EntityImpl<>("sex", "male"));
        User user3 = new User("Arty", "adklas@safa.sa", 24, new EntityImpl<>("sex", "male"));
        User user4 = new User("Max", "adklas@safa.sa", 25, new EntityImpl<>("sex", "male"));
        User user5 = new User("Helen", "adklas@safa.sa", 26, new EntityImpl<>("sex", "female"));
        User user6 = new User("Susan", "adklas@safa.sa", 27, new EntityImpl<>("sex", "female"));
        User user7 = new User("Barbara", "adklas@safa.sa", 28, new EntityImpl<>("sex", "female"));
        User user8 = new User("Maria", "adklas@safa.sa", 29, new EntityImpl<>("sex", "female"));
        User user9 = new User("Vika", "adklas@safa.sa", 30, new EntityImpl<>("sex", "female"));
        User user10 = new User("Nika", "adklas@safa.sa", 18, new EntityImpl<>("sex", "female"));

        int index2 = arrayListUser.add(user1);
        arrayListUser.add(user2);
        arrayListUser.add(user3);
        arrayListUser.add(user4);
        arrayListUser.add(user5);

        Iterator iterator2 = arrayList.getIterator();
        while (iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
        System.out.println();

        System.out.println("add: " + index);

        linkedListUser.add(user6);
        linkedListUser.add(user7);
        linkedListUser.add(user5);
        linkedListUser.add(user9);
        linkedListUser.add(user10);

        User set2 = arrayListUser.set(0, user7);

        System.out.println("set: " + set2);

        User remove2 = arrayListUser.remove(2);

        System.out.println("remove: " + remove2);

        int find2 = arrayListUser.find(user2);

        System.out.println("find: " + find2);

        User get2 = arrayListUser.get(0);

        System.out.println("get: " + get2);

        arrayList.add(null);

        arrayList.trim();
        System.out.println("trim: " + Arrays.toString(arrayList.toArray()));

        arrayListUser.addAll(linkedListUser);
        System.out.println("add all: " + Arrays.toString(arrayList.toArray()));

        Object[] toArray2 = arrayListUser.toArray(linkedListUser.toArray());
        System.out.println("to array: " + Arrays.toString(toArray2));

        arrayListUser.setMaxSize(5);
        System.out.println("set max size: " + Arrays.toString(arrayListUser.toArray()));

        System.out.println("get max size: " + arrayListUser.getMaxSize());

        arrayListUser.clear();
        System.out.println("clear and size: " + arrayListUser.size());

        arrayList.clear();

        arrayListUser.add(user1);
        arrayListUser.add(user2);
        arrayListUser.add(user3);
        arrayListUser.add(user4);
        arrayListUser.add(user5);

        arrayListUser.filterMatches(linkedListUser);
        System.out.println("filter matches: " + Arrays.toString(arrayListUser.toArray()));

        arrayListUser.clear();

        arrayListUser.add(user1);
        arrayListUser.add(user2);
        arrayListUser.add(user3);
        arrayListUser.add(user4);
        arrayListUser.add(user5);

        arrayListUser.filterDifference(linkedListUser);
        System.out.println("filter difference: " + Arrays.toString(arrayListUser.toArray()));
    }
}

