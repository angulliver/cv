package com.epam.vinnik;

import com.epam.vinnik.collections.map.EntityImpl;
import com.epam.vinnik.collections.queue.ArrayQueue;
import com.epam.vinnik.collections.queue.ListQueue;
import com.epam.vinnik.iterator.Iterator;

import java.util.Arrays;

public class ListQueueDemo {
    public static void main(String[] args) {
        ListQueue<Integer> listQueue = new ListQueue<>();
        ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();

        listQueue.push(1);
        listQueue.push(2);
        listQueue.push(3);
        listQueue.push(4);
        listQueue.push(5);

        Iterator iterator = listQueue.getIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        arrayQueue.push(6);
        arrayQueue.push(7);
        arrayQueue.push(4);
        arrayQueue.push(9);
        arrayQueue.push(10);

        boolean isEmpty = listQueue.isEmpty();
        System.out.println("is empty: " + isEmpty);

        Integer peek = listQueue.peek();
        System.out.println("peek: " + peek);

        Integer pull = listQueue.pull();
        System.out.println("pull: " + pull);

        Integer poll = listQueue.poll();
        System.out.println("poll: " + poll);

        Integer remove = listQueue.remove();
        System.out.println("remove: " + remove);

        listQueue.pushAll(arrayQueue);
        System.out.println("push all: " + Arrays.toString(listQueue.toArray()));

        int search = listQueue.search(2);
        System.out.println("search: " + search);

        int clear = listQueue.clear();
        System.out.println("clear: " + clear);

        int size = listQueue.size();
        System.out.println("size: " + size);


        System.out.println("\n\n\ntest with com.epam.vinnik.User: \n");

        ArrayQueue<User> arrayQueueUser = new ArrayQueue<>();
        ListQueue<User> listQueueUser = new ListQueue<>();

        User user1 = new User("Alex", "adklas@safa.sa", 22, new EntityImpl<>("sex", "male"));
        User user2 = new User("Nik", "adklas@safa.sa", 23, new EntityImpl<>("sex", "male"));
        User user3 = new User("Arty", "adklas@safa.sa", 24, new EntityImpl<>("sex", "male"));
        User user4 = new User("Max", "adklas@safa.sa", 25, new EntityImpl<>("sex", "male"));
        User user5 = new User("Helen", "adklas@safa.sa", 26, new EntityImpl<>("sex", "female"));
        User user6 = new User("Susan", "adklas@safa.sa", 27, new EntityImpl<>("sex", "female"));
        User user7 = new User("Barbara", "adklas@safa.sa", 28, new EntityImpl<>("sex", "female"));
        User user8 = new User("Maria", "adklas@safa.sa", 29, new EntityImpl<>("sex", "female"));
        User user9 = new User("Vika", "adklas@safa.sa", 30, new EntityImpl<>("sex", "female"));
        User user10 = new User("Nika", "adklas@safa.sa", 18, new EntityImpl<>("sex", "female"));

        listQueueUser.push(user1);
        listQueueUser.push(user2);
        listQueueUser.push(user3);
        listQueueUser.push(user4);
        listQueueUser.push(user5);

        Iterator iterator2 = listQueueUser.getIterator();
        while (iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
        System.out.println();

        arrayQueueUser.push(user6);
        arrayQueueUser.push(user7);
        arrayQueueUser.push(user8);
        arrayQueueUser.push(user9);
        arrayQueueUser.push(user10);

        boolean isEmpty2 = listQueueUser.isEmpty();
        System.out.println("is empty: " + isEmpty2);

        User peek2 = listQueueUser.peek();
        System.out.println("peek: " + peek2);

        User pull2 = listQueueUser.pull();
        System.out.println("pull: " + pull2);

        User poll2 = listQueueUser.poll();
        System.out.println("poll: " + poll2);

        User remove2 = listQueueUser.remove();
        System.out.println("remove: " + remove2);

        listQueueUser.pushAll(arrayQueueUser);
        System.out.println("push all: " + Arrays.toString(arrayQueueUser.toArray()));

        int search2 = listQueueUser.search(user2);
        System.out.println("search: " + search2);

        int clear2 = listQueueUser.clear();
        System.out.println("clear: " + clear2);

        int size2 = listQueueUser.size();
        System.out.println("size: " + size2);
    }
}
