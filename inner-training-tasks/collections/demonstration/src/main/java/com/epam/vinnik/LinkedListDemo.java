package com.epam.vinnik;

import com.epam.vinnik.collections.list.ArrayList;
import com.epam.vinnik.collections.list.LinkedList;
import com.epam.vinnik.collections.map.EntityImpl;
import com.epam.vinnik.iterator.Iterator;

import java.util.Arrays;

public class LinkedListDemo {
    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();
        ArrayList<Integer> arrayList = new ArrayList<>();

        int index = linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(5);

        Iterator iterator = linkedList.getIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        System.out.println("add, index of element: " + index);

        arrayList.add(6);
        arrayList.add(7);
        arrayList.add(5);
        arrayList.add(9);
        arrayList.add(10);

        Integer set = linkedList.set(0, 10);

        System.out.println("set, old element: " + set);

        Integer remove = linkedList.remove(2);

        System.out.println("remove, removed element: " + remove);

        int find = linkedList.find(10);

        System.out.println("find, index: " + find);

        Integer get = linkedList.get(0);

        System.out.println("get, element: " + get);

        linkedList.add(null);

        linkedList.trim();
        System.out.println("trim: " + Arrays.toString(linkedList.toArray()));

        linkedList.addAll(arrayList);
        System.out.println("add all: " + Arrays.toString(linkedList.toArray()));

        Object[] toArray = linkedList.toArray(arrayList.toArray());
        System.out.println("to array: " + Arrays.toString(toArray));

        linkedList.setMaxSize(5);
        System.out.println("set max size: " + Arrays.toString(linkedList.toArray()));

        System.out.println("get max size: " + linkedList.getMaxSize());

        linkedList.clear();
        System.out.println("clear and size: " + linkedList.size());

        linkedList.clear();

        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(5);

        linkedList.filterMatches(arrayList);
        System.out.println("filter matches: " + Arrays.toString(linkedList.toArray()));

        linkedList.clear();

        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(5);

        linkedList.filterDifference(arrayList);
        System.out.println("filter difference: " + Arrays.toString(linkedList.toArray()));


        System.out.println("\n\n\ntest with com.epam.vinnik.User: \n");

        LinkedList<User> linkedListUser = new LinkedList<>();
        ArrayList<User> arrayListUser = new ArrayList<>();

        User user1 = new User("Alex", "adklas@safa.sa", 22, new EntityImpl<>("sex", "male"));
        User user2 = new User("Nik", "adklas@safa.sa", 23, new EntityImpl<>("sex", "male"));
        User user3 = new User("Arty", "adklas@safa.sa", 24, new EntityImpl<>("sex", "male"));
        User user4 = new User("Max", "adklas@safa.sa", 25, new EntityImpl<>("sex", "male"));
        User user5 = new User("Helen", "adklas@safa.sa", 26, new EntityImpl<>("sex", "female"));
        User user6 = new User("Susan", "adklas@safa.sa", 27, new EntityImpl<>("sex", "female"));
        User user7 = new User("Barbara", "adklas@safa.sa", 28, new EntityImpl<>("sex", "female"));
        User user8 = new User("Maria", "adklas@safa.sa", 29, new EntityImpl<>("sex", "female"));
        User user9 = new User("Vika", "adklas@safa.sa", 30, new EntityImpl<>("sex", "female"));
        User user10 = new User("Nika", "adklas@safa.sa", 18, new EntityImpl<>("sex", "female"));

        int index2 = linkedListUser.add(user1);
        linkedListUser.add(user2);
        linkedListUser.add(user3);
        linkedListUser.add(user4);
        linkedListUser.add(user5);

        Iterator iterator2 = linkedListUser.getIterator();
        while (iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
        System.out.println();

        System.out.println("add, index of element: " + index);

        arrayListUser.add(user6);
        arrayListUser.add(user7);
        arrayListUser.add(user5);
        arrayListUser.add(user9);
        arrayListUser.add(user10);

        User set2 = linkedListUser.set(0, user7);

        System.out.println("set, old element: " + set2);

        User remove2 = linkedListUser.remove(2);

        System.out.println("remove, removed element: " + remove2);

        int find2 = linkedListUser.find(user3);

        System.out.println("find, index: " + find2);

        User get2 = linkedListUser.get(0);

        System.out.println("get, element: " + get2);

        linkedListUser.add(null);

        linkedListUser.trim();
        System.out.println("trim: " + Arrays.toString(linkedListUser.toArray()));

        linkedListUser.addAll(arrayListUser);
        System.out.println("add all: " + Arrays.toString(linkedListUser.toArray()));

        Object[] toArray2 = linkedListUser.toArray(arrayList.toArray());
        System.out.println("to array: " + Arrays.toString(toArray2));

        linkedListUser.setMaxSize(5);
        System.out.println("set max size: " + Arrays.toString(linkedListUser.toArray()));

        System.out.println("get max size: " + linkedListUser.getMaxSize());

        linkedListUser.clear();
        System.out.println("clear and size: " + linkedListUser.size());

        linkedListUser.clear();

        linkedListUser.add(user1);
        linkedListUser.add(user2);
        linkedListUser.add(user3);
        linkedListUser.add(user4);
        linkedListUser.add(user5);

        linkedListUser.filterMatches(arrayListUser);
        System.out.println("filter matches: " + Arrays.toString(linkedListUser.toArray()));

        linkedListUser.clear();

        linkedListUser.add(user1);
        linkedListUser.add(user2);
        linkedListUser.add(user3);
        linkedListUser.add(user4);
        linkedListUser.add(user5);

        linkedListUser.filterDifference(arrayListUser);
        System.out.println("filter difference: " + Arrays.toString(linkedListUser.toArray()));
    }
}
