package com.epam.vinnik;

import com.epam.vinnik.collections.map.EntityImpl;
import com.epam.vinnik.collections.queue.ArrayQueue;
import com.epam.vinnik.collections.queue.ListQueue;
import com.epam.vinnik.iterator.Iterator;

import java.util.Arrays;

public class ArrayQueueDemo {
    public static void main(String[] args) {
        ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
        ListQueue<Integer> listQueue = new ListQueue<>();

        arrayQueue.push(1);
        arrayQueue.push(2);
        arrayQueue.push(3);
        arrayQueue.push(4);
        arrayQueue.push(5);

        Iterator iterator = arrayQueue.getIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next());
        }
        System.out.println();

        listQueue.push(6);
        listQueue.push(7);
        listQueue.push(4);
        listQueue.push(9);
        listQueue.push(10);

        boolean isEmpty = arrayQueue.isEmpty();
        System.out.println("is empty: " + isEmpty);

        Integer peek = arrayQueue.peek();
        System.out.println("peek: " + peek);

        Integer pull = arrayQueue.pull();
        System.out.println("pull: " + pull);

        Integer poll = arrayQueue.poll();
        System.out.println("poll: " + poll);

        Integer remove = arrayQueue.remove();
        System.out.println("remove: " + remove);

        arrayQueue.pushAll(listQueue);
        System.out.println("push all: " + Arrays.toString(arrayQueue.toArray()));

        int search = arrayQueue.search(2);
        System.out.println("search: " + search);

        int clear = arrayQueue.clear();
        System.out.println("clear: " + clear);

        int size = arrayQueue.size();
        System.out.println("size: " + size);


        System.out.println("\n\n\ntest with com.epam.vinnik.User: \n");

        ArrayQueue<User> arrayQueueUser = new ArrayQueue<>();
        ListQueue<User> listQueueUser = new ListQueue<>();
        User user1 = new User("Alex", "adklas@safa.sa", 22, new EntityImpl<>("sex", "male"));
        User user2 = new User("Nik", "adklas@safa.sa", 23, new EntityImpl<>("sex", "male"));
        User user3 = new User("Arty", "adklas@safa.sa", 24, new EntityImpl<>("sex", "male"));
        User user4 = new User("Max", "adklas@safa.sa", 25, new EntityImpl<>("sex", "male"));
        User user5 = new User("Helen", "adklas@safa.sa", 26, new EntityImpl<>("sex", "female"));
        User user6 = new User("Susan", "adklas@safa.sa", 27, new EntityImpl<>("sex", "female"));
        User user7 = new User("Barbara", "adklas@safa.sa", 28, new EntityImpl<>("sex", "female"));
        User user8 = new User("Maria", "adklas@safa.sa", 29, new EntityImpl<>("sex", "female"));
        User user9 = new User("Vika", "adklas@safa.sa", 30, new EntityImpl<>("sex", "female"));
        User user10 = new User("Nika", "adklas@safa.sa", 18, new EntityImpl<>("sex", "female"));

        arrayQueueUser.push(user1);
        arrayQueueUser.push(user2);
        arrayQueueUser.push(user3);
        arrayQueueUser.push(user4);
        arrayQueueUser.push(user5);

        Iterator iterator2 = arrayQueueUser.getIterator();
        while (iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
        System.out.println();

        listQueueUser.push(user6);
        listQueueUser.push(user7);
        listQueueUser.push(user8);
        listQueueUser.push(user9);
        listQueueUser.push(user10);

        boolean isEmpty2 = arrayQueueUser.isEmpty();
        System.out.println("is empty: " + isEmpty2);

        User peek2 = arrayQueueUser.peek();
        System.out.println("peek: " + peek2);

        User pull2 = arrayQueueUser.pull();
        System.out.println("pull: " + pull2);

        User poll2 = arrayQueueUser.poll();
        System.out.println("poll: " + poll2);

        User remove2 = arrayQueueUser.remove();
        System.out.println("remove: " + remove2);

        arrayQueueUser.pushAll(listQueueUser);
        System.out.println("push all: " + Arrays.toString(arrayQueueUser.toArray()));

        int search2 = arrayQueueUser.search(user2);
        System.out.println("search: " + search2);

        int clear2 = arrayQueueUser.clear();
        System.out.println("clear: " + clear2);

        int size2 = arrayQueueUser.size();
        System.out.println("size: " + size2);
    }
}
