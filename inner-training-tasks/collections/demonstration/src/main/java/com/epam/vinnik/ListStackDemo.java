package com.epam.vinnik;

import com.epam.vinnik.collections.map.EntityImpl;
import com.epam.vinnik.collections.stack.ArrayStack;
import com.epam.vinnik.collections.stack.ListStack;
import com.epam.vinnik.iterator.Iterator;

import java.util.Arrays;

public class ListStackDemo {
    public static void main(String[] args) {
        ListStack<Integer> listStack = new ListStack<>();
        ArrayStack<Integer> arrayStack = new ArrayStack<>();

        listStack.push(1);
        listStack.push(22);
        listStack.push(3);
        listStack.push(4);
        listStack.push(5);

        Iterator iterator = listStack.getIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        arrayStack.push(6);
        arrayStack.push(7);
        arrayStack.push(4);
        arrayStack.push(9);
        arrayStack.push(10);

        boolean isEmpty = listStack.isEmpty();
        System.out.println("is empty: " + isEmpty);

        Integer pop = listStack.pop();
        System.out.println("pop: " + pop);

        Integer peek = listStack.peek();
        System.out.println("peek: " + peek);

        int search = listStack.search(3);
        System.out.println("search: " + search);

        listStack.pushAll(arrayStack);
        System.out.println("push all: " + Arrays.toString(listStack.toArray()));

        int clear = listStack.clear();
        System.out.println("clear, number of removed: " + clear);

        System.out.println("size: " + listStack.size());


        System.out.println("\n\n\ntest with com.epam.vinnik.User: \n");

        ArrayStack<User> arrayStackUser = new ArrayStack<>();
        ListStack<User> listStackUser = new ListStack<>();
        User user1 = new User("Alex", "adklas@safa.sa", 22, new EntityImpl<>("sex", "male"));
        User user2 = new User("Nik", "adklas@safa.sa", 23, new EntityImpl<>("sex", "male"));
        User user3 = new User("Arty", "adklas@safa.sa", 24, new EntityImpl<>("sex", "male"));
        User user4 = new User("Max", "adklas@safa.sa", 25, new EntityImpl<>("sex", "male"));
        User user5 = new User("Helen", "adklas@safa.sa", 26, new EntityImpl<>("sex", "female"));
        User user6 = new User("Susan", "adklas@safa.sa", 27, new EntityImpl<>("sex", "female"));
        User user7 = new User("Barbara", "adklas@safa.sa", 28, new EntityImpl<>("sex", "female"));
        User user8 = new User("Maria", "adklas@safa.sa", 29, new EntityImpl<>("sex", "female"));
        User user9 = new User("Vika", "adklas@safa.sa", 30, new EntityImpl<>("sex", "female"));
        User user10 = new User("Nika", "adklas@safa.sa", 18, new EntityImpl<>("sex", "female"));

        listStackUser.push(user1);
        listStackUser.push(user2);
        listStackUser.push(user3);
        listStackUser.push(user4);
        listStackUser.push(user5);

        Iterator iteratorUser = listStackUser.getIterator();
        while (iteratorUser.hasNext()) {
            System.out.print(iteratorUser.next() + " ");
        }
        System.out.println();

        arrayStackUser.push(user6);
        arrayStackUser.push(user7);
        arrayStackUser.push(user4);
        arrayStackUser.push(user9);
        arrayStackUser.push(user10);

        boolean isEmpty2 = listStackUser.isEmpty();
        System.out.println("is empty: " + isEmpty2);

        User pop2 = listStackUser.pop();
        System.out.println("pop: " + pop2);

        User peek2 = listStackUser.peek();
        System.out.println("peek: " + peek2);

        int search2 = listStackUser.search(user3);
        System.out.println("search: " + search2);

        listStackUser.pushAll(arrayStackUser);
        System.out.println("push all: " + Arrays.toString(listStackUser.toArray()));

        int clear2 = listStackUser.clear();
        System.out.println("clear, number of removed: " + clear2);

        System.out.println("size: " + listStackUser.size());
    }
}
