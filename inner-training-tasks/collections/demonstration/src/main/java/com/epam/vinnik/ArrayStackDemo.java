package com.epam.vinnik;

import com.epam.vinnik.collections.map.EntityImpl;
import com.epam.vinnik.collections.stack.ArrayStack;
import com.epam.vinnik.collections.stack.ListStack;
import com.epam.vinnik.iterator.Iterator;

import java.util.Arrays;

public class ArrayStackDemo {
    public static void main(String[] args) {
        ArrayStack<Integer> arrayStack = new ArrayStack<>();
        ListStack<Integer> listStack = new ListStack<>();

        arrayStack.push(1);
        arrayStack.push(2);
        arrayStack.push(3);
        arrayStack.push(4);
        arrayStack.push(5);

        Iterator iterator = arrayStack.getIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        listStack.push(6);
        listStack.push(7);
        listStack.push(4);
        listStack.push(9);
        listStack.push(10);

        boolean isEmpty = arrayStack.isEmpty();
        System.out.println("is empty: " + isEmpty);

        Integer pop = arrayStack.pop();
        System.out.println("pop: " + pop);

        Integer peek = arrayStack.peek();
        System.out.println("peek: " + peek);

        int search = arrayStack.search(3);
        System.out.println("search: " + search);

        arrayStack.pushAll(listStack);
        System.out.println("push all: " + Arrays.toString(arrayStack.toArray()));

        int clear = arrayStack.clear();
        System.out.println("clear, number of removed: " + clear);

        System.out.println("size: " + arrayStack.size());


        System.out.println("\n\n\ntest with com.epam.vinnik.User: \n");

        ArrayStack<User> arrayStackUser = new ArrayStack<>();
        ListStack<User> listStackUser = new ListStack<>();
        User user1 = new User("Alex", "adklas@safa.sa", 22, new EntityImpl<>("sex", "male"));
        User user2 = new User("Nik", "adklas@safa.sa", 23, new EntityImpl<>("sex", "male"));
        User user3 = new User("Arty", "adklas@safa.sa", 24, new EntityImpl<>("sex", "male"));
        User user4 = new User("Max", "adklas@safa.sa", 25, new EntityImpl<>("sex", "male"));
        User user5 = new User("Helen", "adklas@safa.sa", 26, new EntityImpl<>("sex", "female"));
        User user6 = new User("Susan", "adklas@safa.sa", 27, new EntityImpl<>("sex", "female"));
        User user7 = new User("Barbara", "adklas@safa.sa", 28, new EntityImpl<>("sex", "female"));
        User user8 = new User("Maria", "adklas@safa.sa", 29, new EntityImpl<>("sex", "female"));
        User user9 = new User("Vika", "adklas@safa.sa", 30, new EntityImpl<>("sex", "female"));
        User user10 = new User("Nika", "adklas@safa.sa", 18, new EntityImpl<>("sex", "female"));

        arrayStackUser.push(user1);
        arrayStackUser.push(user2);
        arrayStackUser.push(user3);
        arrayStackUser.push(user4);
        arrayStackUser.push(user5);

        Iterator iteratorUser = arrayStackUser.getIterator();
        while (iteratorUser.hasNext()) {
            System.out.print(iteratorUser.next() + " ");
        }
        System.out.println();

        listStackUser.push(user6);
        listStackUser.push(user7);
        listStackUser.push(user4);
        listStackUser.push(user9);
        listStackUser.push(user10);

        boolean isEmpty2 = arrayStackUser.isEmpty();
        System.out.println("is empty: " + isEmpty2);

        User pop2 = arrayStackUser.pop();
        System.out.println("pop: " + pop2);

        User peek2 = arrayStackUser.peek();
        System.out.println("peek: " + peek2);

        int search2 = arrayStackUser.search(user3);
        System.out.println("search: " + search2);

        arrayStackUser.pushAll(listStackUser);
        System.out.println("push all: " + Arrays.toString(arrayStackUser.toArray()));

        int clear2 = arrayStackUser.clear();
        System.out.println("clear, number of removed: " + clear2);

        System.out.println("size: " + arrayStackUser.size());
    }
}
