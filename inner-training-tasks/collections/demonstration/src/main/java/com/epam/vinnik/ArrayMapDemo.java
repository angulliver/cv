package com.epam.vinnik;

import com.epam.vinnik.collections.list.List;
import com.epam.vinnik.collections.map.ArrayMap;
import com.epam.vinnik.collections.map.Entity;
import com.epam.vinnik.collections.map.EntityImpl;

import java.util.Arrays;

public class ArrayMapDemo {
    public static void main(String[] args) {
        ArrayMap<String, Integer> arrayMap = new ArrayMap<>();
        Entity<String, Integer> entity1 = new EntityImpl<>("one", 1);
        Entity<String, Integer> entity2 = new EntityImpl<>("two", 2);
        Entity<String, Integer> entity3 = new EntityImpl<>("three", 3);
        Entity<String, Integer> entity4 = new EntityImpl<>("four", 4);
        Entity<String, Integer> entity5 = new EntityImpl<>("five", 5);


        arrayMap.set(entity1);
        arrayMap.set(entity2);
        arrayMap.set(entity3);
        arrayMap.set(entity4);
        arrayMap.set(entity5);
        arrayMap.set("six", 6);

        boolean isEmpty = arrayMap.isEmpty();
        System.out.println("is empty: " + isEmpty);

        Entity removeKey = arrayMap.remove("four");
        Entity removeEntity = arrayMap.remove(entity3);
        System.out.println("remove by key: " + removeKey);
        System.out.println("remove by Entity: " + removeEntity);

        List<String> keysList;
        keysList = arrayMap.getKeys();
        System.out.println("keys: " + Arrays.toString(keysList.toArray()));

        List<Integer> valuesList;
        valuesList = arrayMap.getValues();
        System.out.println("values: " + Arrays.toString(valuesList.toArray()));

        Integer getValue = arrayMap.get("two");
        System.out.println("get value: " + getValue);

        Entity getEntity = arrayMap.getEntity("two");
        System.out.println("get Entity: " + getEntity.toString());

        boolean contains = arrayMap.contains(3);
        System.out.println("contains: " + contains);

        int clear = arrayMap.clear();
        System.out.println("clear: " + clear);

        int size = arrayMap.size();
        System.out.println("size: " + size);


        System.out.println("\n\n\ntest with com.epam.vinnik.User: \n");

        ArrayMap<String, User> stringUserArrayMap = new ArrayMap<>();

        User user1 = new User("Alex", "adklas@safa.sa", 22, new EntityImpl<>("sex", "male"));
        User user2 = new User("Nik", "adklas@safa.sa", 23, new EntityImpl<>("sex", "male"));
        User user3 = new User("Arty", "adklas@safa.sa", 24, new EntityImpl<>("sex", "male"));
        User user4 = new User("Max", "adklas@safa.sa", 25, new EntityImpl<>("sex", "male"));
        User user5 = new User("Helen", "adklas@safa.sa", 26, new EntityImpl<>("sex", "female"));

        Entity<String, User> entityForRemove = new EntityImpl<>("user one", user1);

        stringUserArrayMap.set("user one", user1);
        stringUserArrayMap.set("user two", user2);
        stringUserArrayMap.set("user three", user3);
        stringUserArrayMap.set("user four", user4);
        stringUserArrayMap.set("user five", user5);

        boolean isEmpty2 = stringUserArrayMap.isEmpty();
        System.out.println("is empty: " + isEmpty2);

        Entity removeKey2 = stringUserArrayMap.remove("user five");
        Entity removeEntity2 = stringUserArrayMap.remove(entityForRemove);
        System.out.println("remove by key: " + removeKey2);
        System.out.println("remove by Entity: " + removeEntity2);

        List<String> keysList2;
        keysList2 = stringUserArrayMap.getKeys();
        System.out.println("keys: " + Arrays.toString(keysList2.toArray()));

        List<User> valuesList2;
        valuesList2 = stringUserArrayMap.getValues();
        System.out.println("values: " + Arrays.toString(valuesList2.toArray()));

        User getValue2 = stringUserArrayMap.get("user four");
        System.out.println("get value: " + getValue2.toString());

        Entity getEntity2 = stringUserArrayMap.getEntity("user two");
        System.out.println("get Entity: " + getEntity2.toString());

        boolean contains2 = stringUserArrayMap.contains(user3);
        System.out.println("contains: " + contains2);

        int clear2 = stringUserArrayMap.clear();
        System.out.println("clear: " + clear2);

        int size2 = stringUserArrayMap.size();
        System.out.println("size: " + size2);
    }
}
