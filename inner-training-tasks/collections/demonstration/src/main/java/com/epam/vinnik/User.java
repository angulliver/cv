package com.epam.vinnik;

import com.epam.vinnik.collections.map.Entity;

import java.util.Objects;

public class User {
    private String name;
    private String email;
    private Integer age;
    private Entity sex;

    public User(String name, String email, Integer age, Entity sex) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Entity getSex() {
        return sex;
    }

    public void setSex(Entity sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "com.epam.vinnik.User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getName(), user.getName()) &&
                Objects.equals(getEmail(), user.getEmail()) &&
                Objects.equals(getAge(), user.getAge()) &&
                Objects.equals(getSex(), user.getSex());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getEmail(), getAge());
    }
}
