package com.epam.vinnik;

import com.epam.vinnik.collections.list.List;
import com.epam.vinnik.collections.map.Entity;
import com.epam.vinnik.collections.map.EntityImpl;
import com.epam.vinnik.collections.map.ListMap;

import java.util.Arrays;

public class ListMapDemo {
    public static void main(String[] args) {
        ListMap<String, Integer> listMap = new ListMap<>();
        Entity<String, Integer> entity1 = new EntityImpl<>("one", 1);
        Entity<String, Integer> entity2 = new EntityImpl<>("two", 2);
        Entity<String, Integer> entity3 = new EntityImpl<>("three", 3);
        Entity<String, Integer> entity4 = new EntityImpl<>("four", 4);
        Entity<String, Integer> entity5 = new EntityImpl<>("five", 5);


        listMap.set(entity1);
        listMap.set(entity2);
        listMap.set(entity3);
        listMap.set(entity4);
        listMap.set(entity5);
        listMap.set("six", 6);

        boolean isEmpty = listMap.isEmpty();
        System.out.println("is empty: " + isEmpty);

        Entity removeKey = listMap.remove("four");
        Entity removeEntity = listMap.remove(entity3);
        System.out.println("remove by key: " + removeKey);
        System.out.println("remove by Entity: " + removeEntity);

        List<String> keysList;
        keysList = listMap.getKeys();
        System.out.println("keys: " + Arrays.toString(keysList.toArray()));

        List<Integer> valuesList;
        valuesList = listMap.getValues();
        System.out.println("values: " + Arrays.toString(valuesList.toArray()));

        Integer getValue = listMap.get("two");
        System.out.println("get value: " + getValue);

        Entity getEntity = listMap.getEntity("two");
        System.out.println("get Entity: " + getEntity.toString());

        boolean contains = listMap.contains(3);
        System.out.println("contains: " + contains);

        int clear = listMap.clear();
        System.out.println("clear: " + clear);

        int size = listMap.size();
        System.out.println("size: " + size);


        System.out.println("\n\n\ntest with com.epam.vinnik.User: \n");

        ListMap<String, User> stringUserListMap = new ListMap<>();

        User user1 = new User("Alex", "adklas@safa.sa", 22, new EntityImpl<>("sex", "male"));
        User user2 = new User("Nik", "adklas@safa.sa", 23, new EntityImpl<>("sex", "male"));
        User user3 = new User("Arty", "adklas@safa.sa", 24, new EntityImpl<>("sex", "male"));
        User user4 = new User("Max", "adklas@safa.sa", 25, new EntityImpl<>("sex", "male"));
        User user5 = new User("Helen", "adklas@safa.sa", 26, new EntityImpl<>("sex", "female"));

        Entity<String, User> entityForRemove = new EntityImpl<>("user one", user1);

        stringUserListMap.set("user one", user1);
        stringUserListMap.set("user two", user2);
        stringUserListMap.set("user three", user3);
        stringUserListMap.set("user four", user4);
        stringUserListMap.set("user five", user5);

        boolean isEmpty2 = stringUserListMap.isEmpty();
        System.out.println("is empty: " + isEmpty2);

        Entity removeKey2 = stringUserListMap.remove("user five");
        Entity removeEntity2 = stringUserListMap.remove(entityForRemove);
        System.out.println("remove by key: " + removeKey2);
        System.out.println("remove by Entity: " + removeEntity2);

        List<String> keysList2;
        keysList2 = stringUserListMap.getKeys();
        System.out.println("keys: " + Arrays.toString(keysList2.toArray()));

        List<User> valuesList2;
        valuesList2 = stringUserListMap.getValues();
        System.out.println("values: " + Arrays.toString(valuesList2.toArray()));

        User getValue2 = stringUserListMap.get("user four");
        System.out.println("get value: " + getValue2.toString());

        Entity getEntity2 = stringUserListMap.getEntity("user two");
        System.out.println("get Entity: " + getEntity2.toString());

        boolean contains2 = stringUserListMap.contains(user3);
        System.out.println("contains: " + contains2);

        int clear2 = stringUserListMap.clear();
        System.out.println("clear: " + clear2);

        int size2 = stringUserListMap.size();
        System.out.println("size: " + size2);
    }
}
