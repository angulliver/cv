package com.epam.vinnik.collections.list;

import com.epam.vinnik.collections.Collection;
import com.epam.vinnik.iterator.Iterator;

import java.util.Comparator;
import java.util.NoSuchElementException;

public class LinkedList<E> implements List<E> {

    private int size = 0;

    private Node<E> first;

    private Node<E> last;

    private int maxSize = Integer.MAX_VALUE - 5;

    private Comparator<E> comparator;

    public LinkedList() {
    }

    public LinkedList(Comparator<E> comparator) {
        this();
        this.comparator = comparator;
    }

    @Override
    public Iterator<E> getIterator() {
        return new LinkedIterator(0);
    }

    public Iterator<E> getIterator(int index) {
        return new LinkedIterator(index);
    }

    private E unlink(Node<E> x) {
        final E element = x.item;
        final Node<E> next = x.next;
        final Node<E> prev = x.prev;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }

        x.item = null;
        size--;
        return element;
    }

    private Node<E> node(int index) {

        if (index < (size >> 1)) {
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            Node<E> x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    @Override
    public int add(E obj) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, obj, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;

        if (this.comparator != null) {
            sort(comparator);
        } else {
            defaultSort();
        }

        return find(obj);
    }

    public int addWithoutSort(E obj) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, obj, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;

        return find(obj);
    }

    @Override
    public void addAll(Collection<? extends E> c) {
        addAll(c.toArray());
    }

    @Override
    public void addAll(E[] a) {
        if ((a.length + size) > maxSize) {
            throw new ArrayIndexOutOfBoundsException("Maximum array size exceeded!");
        }

        for (int i = 0; i < a.length; i++) {
            this.add(a[i]);
        }

        if (this.comparator != null) {
            sort(comparator);
        } else {
            defaultSort();
        }
    }

    public void addAllWithoutSort(E[] a) {
        if ((a.length + size) > maxSize) {
            throw new ArrayIndexOutOfBoundsException("Maximum array size exceeded!");
        }

        for (int i = 0; i < a.length; i++) {
            this.addWithoutSort(a[i]);
        }
    }

    public void addAllWithoutSort(Collection<? extends E> c) {
        addAllWithoutSort(c.toArray());
    }

    @Override
    public E set(int index, E obj) {
        checkElementIndex(index);
        Node<E> x = node(index);
        E oldVal = x.item;
        x.item = obj;

        if (this.comparator != null) {
            sort(comparator);
        } else {
            defaultSort();
        }

        return oldVal;
    }

    public E setWithoutSort(int index, E obj) {
        checkElementIndex(index);
        Node<E> x = node(index);
        E oldVal = x.item;
        x.item = obj;
        return oldVal;
    }

    @Override
    public E remove(int index) {
        checkElementIndex(index);
        return unlink(node(index));
    }

    @Override
    public void clear() {
        for (Node<E> x = first; x != null; ) {
            Node<E> next = x.next;
            x.item = null;
            x.next = null;
            x.prev = null;
            x = next;
        }
        first = last = null;
        size = 0;
    }

    @Override
    public int find(E obj) {
        int index = 0;
        if (obj == null) {
            for (Node<E> x = first; x != null; x = x.next) {
                if (x.item == null)
                    return index;
                index++;
            }
        } else {
            for (Node<E> x = first; x != null; x = x.next) {
                if (obj.equals(x.item))
                    return index;
                index++;
            }
        }
        return -1;
    }

    @Override
    public E get(int index) {
        checkElementIndex(index);
        return node(index).item;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        E[] elementData = this.toArray();
        Object[] array = new Object[a.length + elementData.length];
        System.arraycopy(elementData, 0, array, 0, size);
        System.arraycopy(a, 0, array, size, a.length);
        LinkedList<T> list = new LinkedList<>();
        list.addAll((T[]) array);
        list.trim();
        return list.toArray();
    }

    @Override
    public E[] toArray() {
        Object[] elementData = new Object[size];
        int i = 0;
        for (Node<E> x = first; x != null; x = x.next)
            elementData[i++] = x.item;
        return (E[]) elementData;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void trim() {
        Iterator<E> iterator = getIterator();
        while (iterator.hasNext()) {
            if (iterator.next() == null) {
                iterator.remove();
            }
        }
    }

    @Override
    public void sort(Comparator<E> comparator) {
        LinkedIterator iterator = new LinkedIterator(0);
        int n = size;
        E[] elementData = this.toArray();
        Object temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (comparator.compare(elementData[j - 1], elementData[j]) > 0) {
                    temp = elementData[j - 1];
                    elementData[j - 1] = elementData[j];
                    elementData[j] = (E) temp;
                }
            }
        }

        for (Object e : elementData) {
            iterator.next();
            iterator.set((E) e);
        }


    }

    private void defaultSort() {
        LinkedIterator iterator = new LinkedIterator(0);
        int n = size;
        E[] elementData = this.toArray();
        Object temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (compare(elementData[j - 1], elementData[j]) > 0) {
                    temp = elementData[j - 1];
                    elementData[j - 1] = elementData[j];
                    elementData[j] = (E) temp;
                }
            }
        }

        for (Object e : elementData) {
            iterator.next();
            iterator.set((E) e);
        }
    }

    private int compare(Object o1, Object o2) {
        if (o1 == null && o2 == null) {
            return 0;
        }
        if (o1 == null) {
            return 1;
        }
        if (o2 == null) {
            return -1;
        }
        return o1.hashCode() - o2.hashCode();
    }

    @Override
    public int getMaxSize() {
        return this.maxSize;
    }

    @Override
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;

        if (maxSize < size) {
            LinkedIterator iterator = new LinkedIterator(maxSize);
            while (iterator.hasNext()) {
                iterator.next();
                iterator.remove();
            }
        }
        size = this.maxSize;
    }

    private void checkElementIndex(int index) {
        if (!(index >= 0 && index < size))
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }

    @Override
    public void filterMatches(Collection<E> c) {
        filterMatches(c.toArray());
    }

    @Override
    public void filterMatches(E[] a) {
        LinkedList<E> linkedList = new LinkedList<>();
        Object[] elementData = this.toArray();
        for (Object e : a) {
            for (Object e2 : elementData) {
                if (e.equals(e2)) {
                    linkedList.add((E) e);
                    break;
                }
            }
        }
        this.clear();
        this.addAll(linkedList);
    }

    @Override
    public void filterDifference(Collection<E> c) {
        filterDifference(c.toArray());
    }

    @Override
    public void filterDifference(E[] a) {
        LinkedList<E> linkedList = new LinkedList<>();
        Object[] elementData = this.toArray();

        for (int i = 0; i < size; ++i) {
            boolean found = false;

            for (int j = 0; j < a.length; ++j) {
                if (a[j].equals(elementData[i])) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                linkedList.add((E) elementData[i]);
            }
        }

        linkedList.trim();
        this.clear();
        this.addAll(linkedList);
    }

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    private class LinkedIterator implements Iterator<E> {
        private Node<E> lastReturned;
        private Node<E> next;
        private int nextIndex;

        LinkedIterator(int index) {
            next = (index == size) ? null : node(index);
            nextIndex = index;
        }

        public boolean hasNext() {
            return (nextIndex < size && nextIndex >= 0);
        }

        public E next() {
            if (!hasNext())
                throw new NoSuchElementException();

            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.item;
        }

        public void remove() {
            if (lastReturned == null)
                throw new IllegalStateException();

            Node<E> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned)
                next = lastNext;
            else
                nextIndex--;
            lastReturned = null;
        }

        public void set(E e) {
            if (lastReturned == null)
                throw new IllegalStateException();
            lastReturned.item = e;
        }

        @Override
        public void addBefore(E e) {
            if (lastReturned.prev != null){
                Node<E> node = new Node<>(lastReturned.prev, e,  lastReturned);
                lastReturned.prev = node;
                node.prev.next = node;
            } else {
                lastReturned.prev = new Node<>(lastReturned, e,null);
            }
            size++;
            nextIndex++;
        }

        @Override
        public void addAfter(E e) {
            if (lastReturned.next != null){
                Node<E> node = new Node<>(lastReturned.next, e, lastReturned);
                lastReturned.next = node;
                node.next.prev = node;
            } else {
                lastReturned.prev = new Node<>(null, e, lastReturned);
            }
            size++;
        }
    }
}
