package com.epam.vinnik.collections.stack;

import com.epam.vinnik.collections.Collection;
import com.epam.vinnik.iterator.Iterator;

import java.util.Comparator;

public interface Stack<E> extends Collection<E> {
    Iterator getIterator();

    Boolean isEmpty();

    E peek();

    E pop();

    void push(E obj);

    void pushAll(Collection<? extends E> c);

    void pushAll(E[] a);

    int search(E obj);

    int clear();

    int size();

    void sort(Comparator<E> comparator);
}
