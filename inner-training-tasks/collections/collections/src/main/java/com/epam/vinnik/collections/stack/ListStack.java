package com.epam.vinnik.collections.stack;

import com.epam.vinnik.collections.Collection;
import com.epam.vinnik.collections.list.LinkedList;
import com.epam.vinnik.iterator.Iterator;

import java.util.Comparator;
import java.util.NoSuchElementException;

public class ListStack<E> implements Stack<E> {
    private LinkedList<E> linkedList;

    public ListStack() {
        this.linkedList = new LinkedList<>();
    }

    @Override
    public Iterator<E> getIterator() {
        return new LinkedIterator(linkedList.size() - 1);
    }

    @Override
    public Boolean isEmpty() {
        return linkedList.size() == 0;
    }

    @Override
    public E peek() {
        return linkedList.get(size() - 1);
    }

    @Override
    public E pop() {
        return linkedList.remove(size() - 1);
    }

    @Override
    public void push(E obj) {
        if (linkedList.find(obj) < 0) {
            add(obj);
        } else throw new IllegalStateException("Element " + obj.toString() + " is exist");
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        addAll(c);
    }

    @Override
    public void pushAll(E[] a) {
        addAll(a);
    }

    @Override
    public int search(E obj) {
        return linkedList.find(obj);
    }

    @Override
    public int clear() {
        int count = 0;
        Iterator iterator = linkedList.getIterator();
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
            count++;
        }
        return count;
    }

    @Override
    public int size() {
        return linkedList.size();
    }

    @Override
    public void sort(Comparator<E> comparator) {
        linkedList.sort(comparator);
    }

    @Override
    public int add(E obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Cannot instantiate with nullable type.");
        }
        return linkedList.addWithoutSort(obj);
    }

    @Override
    public E remove(int index) {
        return linkedList.remove(index);
    }

    @Override
    public void addAll(Collection<? extends E> c) {
        addAll(c.toArray());
    }

    @Override
    public void addAll(E[] a) {
        for (E e : a) {
            if (e == null) {
                throw new IllegalArgumentException("Cannot instantiate with nullable type.");
            }
        }
        linkedList.addAllWithoutSort(a);
    }

    @Override
    public E[] toArray() {
        return linkedList.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        for (T e : a) {
            if (e == null) {
                throw new IllegalArgumentException("Cannot instantiate with nullable type.");
            }
        }
        return linkedList.toArray(a);
    }

    private class LinkedIterator implements Iterator<E> {
        private int index;

        LinkedIterator(int index) {
            this.index = index;
        }

        public boolean hasNext() {
            return (index < size() && index >= 0);
        }

        public E next() {
            if (!hasNext())
                throw new NoSuchElementException();
            int currIndex = index;
            index--;
            return linkedList.get(currIndex);
        }

        public void remove() {
            if (index < 0 || index >= size())
                throw new IllegalStateException();

            linkedList.remove(index);
        }

        public void set(E e) {
            if (index < size() && index >= 0)
                throw new IllegalStateException();
            linkedList.set(index, e);
        }

        @Override
        public void addBefore(E e) {
            ListStack.this.getIterator().addBefore(e);
        }

        @Override
        public void addAfter(E e) {
            ListStack.this.getIterator().addAfter(e);
        }
    }
}
