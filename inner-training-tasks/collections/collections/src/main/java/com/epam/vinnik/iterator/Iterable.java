package com.epam.vinnik.iterator;

public interface Iterable<T> {
    Iterator<T> getIterator();
}
