package com.epam.vinnik.collections.queue;

import com.epam.vinnik.collections.Collection;
import com.epam.vinnik.iterator.Iterator;

public interface Queue<E> extends Collection<E> {
    Iterator<E> getIterator();

    Boolean isEmpty();

    E peek();

    E poll();

    E pull();

    E remove();

    void push(E obj);

    void pushAll(Collection<? extends E> c);

    void pushAll(E[] a);

    int search(E obj);

    int clear();

    int size();
}
