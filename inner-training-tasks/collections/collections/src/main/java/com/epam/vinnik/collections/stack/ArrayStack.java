package com.epam.vinnik.collections.stack;

import com.epam.vinnik.collections.Collection;
import com.epam.vinnik.collections.list.ArrayList;
import com.epam.vinnik.iterator.Iterator;

import java.util.Comparator;
import java.util.NoSuchElementException;

public class ArrayStack<E> implements Stack<E> {
    private ArrayList<E> arrayList;

    public ArrayStack() {
        this.arrayList = new ArrayList<>();
    }

    @Override
    public Iterator<E> getIterator() {
        return new ArrIterator(arrayList.size() - 1);
    }

    @Override
    public Boolean isEmpty() {
        return arrayList.size() == 0;
    }

    @Override
    public E peek() {
        return arrayList.get(size() - 1);
    }

    @Override
    public E pop() {
        return arrayList.remove(size() - 1);
    }

    @Override
    public void push(E obj) {
        if (arrayList.find(obj) < 0) {
            add(obj);
        } else throw new IllegalStateException("Element " + obj.toString() + " is exist");
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        addAll(c);
    }

    @Override
    public void pushAll(E[] a) {
        addAll(a);
    }

    @Override
    public int search(E obj) {
        return arrayList.find(obj);
    }

    @Override
    public int clear() {
        int count = 0;
        for (int i = arrayList.size() - 1; i >= 0; i--) {
            arrayList.remove(i);
            count++;
        }
        return count;
    }

    @Override
    public int size() {
        return arrayList.size();
    }

    @Override
    public void sort(Comparator<E> comparator) {
        arrayList.sort(comparator);
    }

    @Override
    public int add(E obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Cannot instantiate with nullable type.");
        }
        return arrayList.addWithoutSort(obj);
    }

    @Override
    public E remove(int index) {
        return arrayList.remove(index);
    }

    @Override
    public void addAll(Collection<? extends E> c) {
        addAll(c.toArray());
    }

    @Override
    public void addAll(E[] a) {
        for (E e : a) {
            if (e == null) {
                throw new IllegalArgumentException("Cannot instantiate with nullable type.");
            }
        }

        arrayList.addAllWithoutSort(a);
    }

    @Override
    public E[] toArray() {
        return arrayList.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        for (T e : a) {
            if (e == null) {
                throw new IllegalArgumentException("Cannot instantiate with nullable type.");
            }
        }
        return arrayList.toArray(a);
    }

    private class ArrIterator implements Iterator<E> {
        int cursor = 0;

        public ArrIterator(int cursor) {
            this.cursor = cursor;
        }

        @Override
        public boolean hasNext() {
            return (cursor < ArrayStack.this.size() && cursor >= 0);
        }

        @Override
        public E next() {
            int i = cursor;
            if (i >= ArrayStack.this.size())
                throw new NoSuchElementException();
//                Object[] elementData = ArrayList.this.elementData;
            cursor = i--;
            return arrayList.get(cursor--);
        }

        @Override
        public void remove() {
            ArrayStack.this.remove(cursor);
        }

        @Override
        public void addBefore(E e) {
            if (cursor == ArrayStack.this.size()) {
                throw new ArrayIndexOutOfBoundsException();
            }
            ArrayStack.this.arrayList.set(cursor + 1, e);
        }

        @Override
        public void addAfter(E e) {
            if (cursor == 0) {
                throw new ArrayIndexOutOfBoundsException();
            }
            ArrayStack.this.arrayList.set(cursor - 1, e);
        }
    }
}
