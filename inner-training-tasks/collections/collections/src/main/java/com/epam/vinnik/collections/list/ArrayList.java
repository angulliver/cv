package com.epam.vinnik.collections.list;

import com.epam.vinnik.collections.Collection;
import com.epam.vinnik.iterator.Iterator;

import java.util.Comparator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E> {

    private static final int INITIAL_CAPACITY = 10;
    private Object[] elementData;
    private int size;
    private int maxSize = Integer.MAX_VALUE - 5;
    private Comparator<E> comparator;

    public ArrayList() {
        elementData = new Object[INITIAL_CAPACITY];
    }

    public ArrayList(Comparator<E> comparator) {
        this.comparator = comparator;
        elementData = new Object[INITIAL_CAPACITY];
    }

    public Iterator<E> getIterator() {
        return new ArrIterator(0);
    }

    public Iterator<E> getIterator(int index) {
        return new ArrIterator(index);
    }

    public int addWithoutSort(E obj) {

        validateCapacity(this.size);
        elementData[size++] = obj;

        return find(obj);
    }

    public int add(E obj) {

        validateCapacity(this.size);
        elementData[size++] = obj;

        if (this.comparator != null) {
            sort(comparator);
        } else {
            defaultSort();
        }

        return find(obj);
    }

    private void validateCapacity(int size) {
        if (size > maxSize) {
            throw new ArrayIndexOutOfBoundsException("Maximum array size exceeded!");
        }

        int oldCapacity = elementData.length;
        if (size >= oldCapacity) {
            Object[] oldData = elementData;
            int newCapacity = (oldCapacity * 3) / 2 + 1; //Size increases by 1.5 times+1.

            if (newCapacity < size)
                newCapacity = size;
            elementData = new Object[newCapacity];
            System.arraycopy(oldData, 0, elementData, 0, this.size);
        }
    }

    public void addAll(Collection<? extends E> c) {
        addAll(c.toArray());
    }

    public void addAll(E[] a) {
        if ((a.length + size) > maxSize) {
            throw new ArrayIndexOutOfBoundsException("Maximum array size exceeded!");
        }

        for (int i = 0; i < a.length; i++) {
            this.add(a[i]);
        }

        if (this.comparator != null) {
            sort(comparator);
        } else {
            defaultSort();
        }
    }

    public void addAllWithoutSort(E[] a) {
        if ((a.length + size) > maxSize) {
            throw new ArrayIndexOutOfBoundsException("Maximum array size exceeded!");
        }

        for (int i = 0; i < a.length; i++) {
            this.addWithoutSort(a[i]);
        }
    }

    public void addAllWithoutSort(Collection<? extends E> c) {
        addAllWithoutSort(c.toArray());
    }

    public E set(int index, E obj) {
        rangeCheck(index);

        E oldValue = (E) this.elementData[index];
        this.elementData[index] = obj;

        if (this.comparator != null) {
            sort(comparator);
        } else {
            defaultSort();
        }

        return oldValue;
    }

    public E remove(int index) {
        rangeCheck(index);

        Object oldValue = elementData[index];

        int numMoved = size - index - 1;
        if (numMoved > 0) {
            System.arraycopy(elementData, index + 1, elementData, index, numMoved);
        }
        elementData[--size] = null;

        return (E) oldValue;
    }

    public E setWithoutSort(int index, E obj) {
        rangeCheck(index);

        E oldValue = (E) this.elementData[index];
        this.elementData[index] = obj;

        return oldValue;
    }

    public void clear() {
        for (int i = 0; i < size; i++)
            elementData[i] = null;

        size = 0;
    }

    public int find(E obj) {
        if (obj == null) {
            for (int i = 0; i < size; i++)
                if (elementData[i] == null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (obj.equals(elementData[i]))
                    return i;
        }
        return -1;
    }

    public E get(int index) {
        rangeCheck(index);

        return (E) elementData[index];
    }

    public <T> T[] toArray(T[] a) {
        Object[] array = new Object[a.length + elementData.length];
        System.arraycopy(this.toArray(), 0, array, 0, size);
        System.arraycopy(a, 0, array, size, a.length);
        ArrayList<T> list = new ArrayList<>();
        list.addAll((T[]) array);
        list.trim();
        return list.toArray();
    }

    public E[] toArray() {
        Object[] arr = new Object[size];
        System.arraycopy(elementData, 0, arr, 0, size);

        return (E[]) arr;
    }

    public int size() {
        return size;
    }

    public void trim() {
        int count = 0;
        for (Object i : elementData) {
            if (i != null) {
                count++;
            }
        }

        Object[] newArray = new Object[count];

        int index = 0;
        for (Object i : elementData) {
            if (i != null) {
                newArray[index++] = i;
            }
        }

        size = newArray.length;
        elementData = newArray;
    }

    public int getMaxSize() {
        return this.maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;

        if (maxSize < size) {
            Object[] arr = new Object[maxSize];
            System.arraycopy(elementData, 0, arr, 0, arr.length);
            elementData = arr;
        }

        size = this.maxSize;
    }

    public void sort(Comparator<E> comparator) {
        int n = size;
        Object temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (comparator.compare((E) elementData[j - 1], (E) elementData[j]) > 0) {
                    temp = elementData[j - 1];
                    elementData[j - 1] = elementData[j];
                    elementData[j] = temp;
                }
            }
        }
    }

    private void defaultSort() {
        int n = size;
        Object temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (compare(elementData[j - 1], elementData[j]) > 0) {
                    temp = elementData[j - 1];
                    elementData[j - 1] = elementData[j];
                    elementData[j] = temp;
                }
            }
        }
    }

    private int compare(Object o1, Object o2) {
        if (o1 == null && o2 == null) {
            return 0;
        }
        if (o1 == null) {
            return 1;
        }
        if (o2 == null) {
            return -1;
        }
        return o1.hashCode() - o2.hashCode();
    }

    private void rangeCheck(int index) {
        if (index < 0 || index >= this.size)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }

    public void filterMatches(Collection<E> c) {
        filterMatches(c.toArray());
    }

    public void filterMatches(E[] a) {
        ArrayList<E> arrayList = new ArrayList<>();
        Object[] data = this.toArray();
        for (Object e : a) {
            for (Object e2 : data) {
                if (e.equals(e2)) {
                    arrayList.add((E) e);
                    break;
                }
            }
        }
        this.clear();
        this.addAll(arrayList);
    }

    public void filterDifference(Collection<E> c) {
        filterDifference(c.toArray());
    }

    public void filterDifference(E[] a) {
        ArrayList<E> arrayList = new ArrayList<>();

        for (int i = 0; i < size; ++i) {
            boolean found = false;

            for (int j = 0; j < a.length; ++j) {
                if (a[j].equals(elementData[i])) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                arrayList.add((E) elementData[i]);
            }
        }

        arrayList.trim();
        this.elementData = arrayList.toArray();
        this.size = this.elementData.length;
    }

    private class ArrIterator implements Iterator<E> {
        int cursor = 0;

        public ArrIterator(int cursor) {
            this.cursor = cursor;
        }

        @Override
        public boolean hasNext() {
            return cursor < ArrayList.this.size;
        }

        @Override
        public E next() {
            int i = cursor;
            if (i >= ArrayList.this.size)
                throw new NoSuchElementException();
//                Object[] elementData = ArrayList.this.elementData;
            cursor = i++;
            return (E) elementData[cursor++];
        }

        @Override
        public void remove() {
            ArrayList.this.remove(cursor);
        }

        @Override
        public void addBefore(E e) {
            if (cursor == 0) {
                throw new ArrayIndexOutOfBoundsException();
            }
            ArrayList.this.set(cursor - 1, e);
        }

        @Override
        public void addAfter(E e) {
            if (cursor == ArrayList.this.size) {
                throw new ArrayIndexOutOfBoundsException();
            }
            ArrayList.this.set(cursor + 1, e);
        }
    }
}
