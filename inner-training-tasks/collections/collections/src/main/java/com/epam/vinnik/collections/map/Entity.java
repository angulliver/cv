package com.epam.vinnik.collections.map;

public interface Entity<K, V> {
    K getKey();

    V getValue();
}
