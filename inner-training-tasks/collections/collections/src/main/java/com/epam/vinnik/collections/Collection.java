package com.epam.vinnik.collections;

import com.epam.vinnik.iterator.Iterable;
import com.epam.vinnik.iterator.Iterator;

public interface Collection<E> extends Iterable<E> {
    Iterator<E> getIterator();

    int size();

    int add(E obj);

    E remove(int index);

    void addAll(Collection<? extends E> c);

    void addAll(E[] a);

    E[] toArray();

    <T> T[] toArray(T[] a);
}
