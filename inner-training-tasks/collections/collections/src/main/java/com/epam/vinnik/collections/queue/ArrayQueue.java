package com.epam.vinnik.collections.queue;

import com.epam.vinnik.collections.Collection;
import com.epam.vinnik.collections.list.ArrayList;
import com.epam.vinnik.iterator.Iterator;

import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Queue<E> {
    private ArrayList arrayList;

    public ArrayQueue() {
        this.arrayList = new ArrayList();
    }

    @Override
    public Iterator<E> getIterator() {
        return new ArrIterator(0);
    }

    @Override
    public Boolean isEmpty() {
        return arrayList.size() == 0;
    }

    @Override
    public E peek() {
        if (arrayList.size() == 0) {
            throw new IndexOutOfBoundsException("Queue is empty!");
        }
        return (E) arrayList.get(0);
    }

    @Override
    public E poll() {
        if (arrayList.size() == 0) {
            throw new IndexOutOfBoundsException("Queue is empty!");
        }
        return (E) arrayList.remove(0);
    }

    @Override
    public E pull() {
        if (arrayList.size() == 0) {
            throw new IndexOutOfBoundsException("Queue is empty!");
        }
        return (E) arrayList.get(size() - 1);
    }

    @Override
    public E remove() {
        if (arrayList.size() == 0) {
            throw new IndexOutOfBoundsException("Queue is empty!");
        }
        return (E) arrayList.remove(size() - 1);
    }

    @Override
    public void push(E obj) {
        add(obj);
    }

    @Override
    public void pushAll(E[] a) {
        addAll(a);
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        addAll(c);
    }

    @Override
    public int search(E obj) {
        return arrayList.find(obj);
    }

    @Override
    public int clear() {
        int count = 0;
        for (int i = arrayList.size() - 1; i >= 0; i--) {
            remove(i);
            count++;
        }
        return count;
    }

    @Override
    public int size() {
        return arrayList.size();
    }

    @Override
    public int add(E obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Cannot instantiate with nullable type.");
        }
        return arrayList.addWithoutSort(obj);
    }

    @Override
    public E remove(int index) {
        return (E) arrayList.remove(index);
    }

    @Override
    public void addAll(Collection<? extends E> c) {
        addAll(c.toArray());
    }

    @Override
    public void addAll(E[] a) {
        for (E e : a) {
            if (e == null) {
                throw new IllegalArgumentException("Cannot instantiate with nullable type.");
            }
        }

        arrayList.addAllWithoutSort(a);
    }

    @Override
    public E[] toArray() {
        return (E[]) arrayList.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        for (T e : a) {
            if (e == null) {
                throw new IllegalArgumentException("Cannot instantiate with nullable type.");
            }
        }
        return (T[]) arrayList.toArray(a);
    }

    private class ArrIterator implements Iterator<E> {
        int cursor = 0;

        public ArrIterator(int cursor) {
            this.cursor = cursor;
        }

        @Override
        public boolean hasNext() {
            return (cursor < ArrayQueue.this.size() && cursor >= 0);
        }

        @Override
        public E next() {
            int i = cursor;
            if (i >= ArrayQueue.this.size())
                throw new NoSuchElementException();
            cursor = i--;
            return (E) arrayList.get(cursor++);
        }

        @Override
        public void remove() {
            ArrayQueue.this.remove(cursor);
        }

        @Override
        public void addBefore(E e) {
            if (cursor == ArrayQueue.this.size()) {
                throw new ArrayIndexOutOfBoundsException();
            }
            ArrayQueue.this.arrayList.set(cursor + 1, e);
        }

        @Override
        public void addAfter(E e) {
            if (cursor == 0) {
                throw new ArrayIndexOutOfBoundsException();
            }
            ArrayQueue.this.arrayList.set(cursor - 1, e);
        }
    }
}

