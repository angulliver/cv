package com.epam.vinnik.collections.queue;

import com.epam.vinnik.collections.Collection;
import com.epam.vinnik.collections.list.LinkedList;
import com.epam.vinnik.iterator.Iterator;

public class ListQueue<E> implements Queue<E> {
    private LinkedList<E> linkedList;

    public ListQueue() {
        this.linkedList = new LinkedList<>();
    }

    @Override
    public Iterator<E> getIterator() {
        return linkedList.getIterator();
    }

    @Override
    public Boolean isEmpty() {
        return linkedList.size() == 0;
    }

    @Override
    public E peek() {
        if (linkedList.size() == 0) {
            throw new IndexOutOfBoundsException("Queue is empty!");
        }
        return linkedList.get(0);
    }

    @Override
    public E poll() {
        if (linkedList.size() == 0) {
            throw new IndexOutOfBoundsException("Queue is empty!");
        }
        return linkedList.remove(0);
    }

    @Override
    public E pull() {
        if (linkedList.size() == 0) {
            throw new IndexOutOfBoundsException("Queue is empty!");
        }
        return linkedList.get(size() - 1);
    }

    @Override
    public E remove() {
        if (linkedList.size() == 0) {
            throw new IndexOutOfBoundsException("Queue is empty!");
        }
        return linkedList.remove(size() - 1);
    }

    @Override
    public void push(E obj) {
        add(obj);
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        addAll(c);
    }

    @Override
    public void pushAll(E[] a) {
        addAll(a);
    }

    @Override
    public int search(E obj) {
        return linkedList.find(obj);
    }

    @Override
    public int clear() {
        int count = 0;
        Iterator iterator = linkedList.getIterator();
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
            count++;
        }
        return count;
    }

    @Override
    public int size() {
        return linkedList.size();
    }

    @Override
    public int add(E obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Cannot instantiate with nullable type.");
        }
        return linkedList.addWithoutSort(obj);
    }

    @Override
    public E remove(int index) {
        return linkedList.remove(index);
    }

    @Override
    public void addAll(Collection<? extends E> c) {
        addAll(c.toArray());
    }

    @Override
    public void addAll(E[] a) {
        for (E e : a) {
            if (e == null) {
                throw new IllegalArgumentException("Cannot instantiate with nullable type.");
            }
        }
        linkedList.addAllWithoutSort(a);
    }

    @Override
    public E[] toArray() {
        return linkedList.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        for (T e : a) {
            if (e == null) {
                throw new IllegalArgumentException("Cannot instantiate with nullable type.");
            }
        }
        return linkedList.toArray(a);
    }
}
