package com.epam.vinnik.collections.map;

import com.epam.vinnik.collections.list.List;

public interface Map<K, V> {
    Boolean isEmpty();

    void set(K key, V value);

    void set(Entity e);

    Entity remove(K key);

    Entity remove(Entity e);

    List getKeys();

    List getValues();

    V get(K key);

    Entity getEntity(K key);

    Boolean contains(V value);

    int clear();

    int size();
}
