package com.epam.vinnik.collections.list;

import com.epam.vinnik.collections.Collection;
import com.epam.vinnik.iterator.Iterator;

import java.util.Comparator;

public interface List<E> extends Collection<E> {
    Iterator<E> getIterator();

    int add(E obj);

    void addAll(Collection<? extends E> c);

    void addAll(E[] a);

    E set(int index, E obj);

    E remove(int index);

    void clear();

    int find(E obj);

    E get(int index);

    <T> T[] toArray(T[] a);

    E[] toArray();

    int size();

    void trim();

    void sort(Comparator<E> comparator);

    void filterMatches(Collection<E> c);

    void filterMatches(E[] a);

    void filterDifference(Collection<E> c);

    void filterDifference(E[] a);

    int getMaxSize();

    void setMaxSize(int maxSize);
}
