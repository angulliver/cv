package com.epam.vinnik.iterator;

public interface Iterator<E> {
    boolean hasNext();

    E next();

    void remove();

    void addBefore(E e);

    void addAfter(E e);
}
