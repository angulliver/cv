create database IF NOT EXISTS sql_homework;

create table if not exists sql_homework.employees
(
  employeeID int auto_increment
    primary key,
  surname    varchar(20) not null,
  name       varchar(20) not null,
  sex        varchar(20) not null,
  birthday   date        not null,
  married    tinyint(1)  not null
);

create table if not exists sql_homework.providers
(
  providerID int auto_increment
    primary key,
  pName      varchar(25) not null,
  TIN        int         not null,
  city       varchar(25) not null
);

create table if not exists sql_homework.materials
(
  materialID int auto_increment
    primary key,
  providerID int         not null,
  mName      varchar(20) not null,
  price      int         not null,
  foreign key (providerID) references providers (providerID)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

create table if not exists sql_homework.departments
(
  departmentID int auto_increment
    primary key,
  dName        varchar(25) not null,
  materialID   int         not null,
  providerID   int         not null,
  employeeID   int         not null,
  foreign key (employeeID) references employees (employeeID)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  foreign key (materialID) references materials (materialID)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  foreign key (providerID) references providers (providerID)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

create table if not exists sql_homework.stocks
(
  stockID      int auto_increment
    primary key,
  sName        varchar(25) not null,
  departmentID int         not null,
  foreign key (departmentID) references departments (departmentID)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

INSERT INTO sql_homework.employees (surname, name, sex, birthday, married)
VALUES ('Vilkov', 'Ehor', 'man', '1970-01-16', 1),
       ('Bolun', 'Maria', 'woman', '1990-05-10', 0),
       ('Holod', 'Max', 'man', '1970-01-22', 1),
       ('Morlod', 'Helen', 'woman', '1970-06-19', 0),
       ('Golum', 'Maria', 'woman', '1991-08-22', 1);

INSERT INTO sql_homework.providers (pName, TIN, city)
VALUES ('Mokos', 852456963, 'Minsk'),
       ('Nokos', 412852456, 'NY'),
       ('Pokos', 456852479, 'Paris'),
       ('Lokos', 741963456, 'London'),
       ('Tokos', 741963789, 'Tokio');

INSERT INTO sql_homework.materials (providerID, mName, price)
VALUES (2, 'wood', 2563),
       (1, 'aluminium', 5698),
       (4, 'copper', 6896),
       (3, 'glass', 2891),
       (5, 'oil', 4222);

INSERT INTO sql_homework.departments (materialID, dName, providerID, employeeID)
VALUES (1, 'Sonol', 2, 3),
       (2, 'Lytaz', 1, 1),
       (3, 'Goplod', 4, 2),
       (4, 'Foltam', 3, 5),
       (5, 'Bolozg', 5, 4);

INSERT INTO sql_homework.stocks (departmentID, sName)
VALUES (1, 'Houio'),
       (2, 'Jdopa'),
       (3, 'Jlyra'),
       (4, 'Lopna'),
       (5, 'Holda');