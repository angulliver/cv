package com.epam.vinnik.dao;

import com.epam.vinnik.dao.impl.*;
import com.epam.vinnik.entity.*;

import java.util.HashMap;
import java.util.Map;

public class EntityDAOFactory {
    private static final String NO_DAO_INSTANCE_IN_FACTORY_FOR_CLASS = "No DAO instance in factory for class";
    private static final Map<Class, Object> DB_DAO = new HashMap<>();

    static {
        DB_DAO.put(Department.class, new DepartmentImpl());
        DB_DAO.put(Employee.class, new EmployeeImpl());
        DB_DAO.put(Material.class, new MaterialImpl());
        DB_DAO.put(Provider.class, new ProviderImpl());
        DB_DAO.put(Stock.class, new StockImpl());
    }

    @SuppressWarnings("unchecked")
    public static <T> T getDAO(Class entityClass) {
        Object dao = DB_DAO.get(entityClass);
        if (dao == null) {
            System.err.println(NO_DAO_INSTANCE_IN_FACTORY_FOR_CLASS + entityClass);
        }
        return (T) dao;
    }
}
