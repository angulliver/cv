package com.epam.vinnik.dao;

public interface EntityDAO<T> {
    void updateName(String name, int id);

    T getByID(int id);

    void add(T obj);

    void deleteById(int id);
}
