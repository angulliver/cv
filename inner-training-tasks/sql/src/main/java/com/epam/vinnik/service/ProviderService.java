package com.epam.vinnik.service;

import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.dao.EntityDAOFactory;
import com.epam.vinnik.entity.Provider;

public class ProviderService {
    private EntityDAO<Provider> providerDAO;

    public ProviderService() {
        this.providerDAO = EntityDAOFactory.getDAO(Provider.class);
    }

    public void updateProviderName(String name, int id) {
        providerDAO.updateName(name, id);
    }

    public Provider getProviderByID(int id) {
        return providerDAO.getByID(id);
    }

    public void addProvider(Provider obj) {
        providerDAO.add(obj);
    }

    public void deleteProviderById(int id) {
        providerDAO.getByID(id);
    }
}
