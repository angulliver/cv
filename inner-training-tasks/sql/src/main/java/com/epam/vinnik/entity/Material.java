package com.epam.vinnik.entity;


public class Material {

    private int materialId;
    private int providerId;
    private String mName;
    private int price;


    public int getMaterialId() {
        return materialId;
    }


    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }


    public String getMName() {
        return mName;
    }

    public void setMName(String mName) {
        this.mName = mName;
    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
