package com.epam.vinnik.dao.impl;

import com.epam.vinnik.connectionTools.ConnectionManager;
import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.entity.Provider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProviderImpl implements EntityDAO<Provider> {
    private Connection connection = ConnectionManager.getConnection();

    @Override
    public void updateName(String name, int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.PROVIDER_UPDATE_NAME)) {
            statement.setString(1, name);
            statement.setInt(2, id);

            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Provider getByID(int id) {
        Provider provider = new Provider();
        try (PreparedStatement statement = getByIdCreatePs(id, Queries.PROVIDER_GET_BY_ID);
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                provider.setPName(resultSet.getString("pName"));
                provider.setTin(resultSet.getInt("TIN"));
                provider.setCity(resultSet.getString("city"));
            } else {
                throw new SQLException("Incorrect query.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return provider;
    }

    private PreparedStatement getByIdCreatePs(int id, String sqlQuery) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        statement.setInt(1, id);

        return statement;
    }

    @Override
    public void add(Provider obj) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.PROVIDER_ADD)) {
            statement.setString(1, obj.getPName());
            statement.setInt(2, obj.getTin());
            statement.setString(3, obj.getCity());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.PROVIDER_DELETE)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
