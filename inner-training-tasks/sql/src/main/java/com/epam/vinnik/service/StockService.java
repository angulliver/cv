package com.epam.vinnik.service;

import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.dao.EntityDAOFactory;
import com.epam.vinnik.entity.Stock;

public class StockService {
    EntityDAO<Stock> stockDAO;

    public StockService() {
        this.stockDAO = EntityDAOFactory.getDAO(Stock.class);
    }

    public void updateStockName(String name, int id) {
        stockDAO.updateName(name, id);
    }

    public Stock getStockByID(int id) {
        return stockDAO.getByID(id);
    }

    public void addStock(Stock obj) {
        stockDAO.add(obj);
    }

    public void deleteStockById(int id) {
        stockDAO.deleteById(id);
    }
}
