package com.epam.vinnik.entity;


public class Provider {

    private int providerId;
    private String pName;
    private int tin;
    private String city;


    public int getProviderId() {
        return providerId;
    }


    public String getPName() {
        return pName;
    }

    public void setPName(String pName) {
        this.pName = pName;
    }


    public int getTin() {
        return tin;
    }

    public void setTin(int tin) {
        this.tin = tin;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
