package com.epam.vinnik.service;

import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.dao.EntityDAOFactory;
import com.epam.vinnik.entity.Material;

public class MaterialService {
    private EntityDAO<Material> materialDAO;

    public MaterialService() {
        this.materialDAO = EntityDAOFactory.getDAO(Material.class);
    }

    public void updateMaterialName(String name, int id) {
        materialDAO.updateName(name, id);
    }

    public Material getMaterialByID(int id) {
        return materialDAO.getByID(id);
    }

    public void addMaterial(Material obj) {
        materialDAO.add(obj);
    }

    public void deleteMaterialById(int id) {
        materialDAO.deleteById(id);
    }
}
