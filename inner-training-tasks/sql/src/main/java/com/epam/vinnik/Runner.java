package com.epam.vinnik;

import com.epam.vinnik.entity.*;
import com.epam.vinnik.service.*;

public class Runner {
    public static void main(String[] args) {
        DepartmentService departmentService = new DepartmentService();
        EmployeeService employeeService = new EmployeeService();
        MaterialService materialService = new MaterialService();
        ProviderService providerService = new ProviderService();
        StockService stockService = new StockService();

        departmentService.deleteDepartmentById(1);
        Department department = departmentService.getDepartmentByID(3);
        departmentService.addDepartment(department);
        departmentService.updateDepartmentName("Test", 3);

        employeeService.deleteEmployeeById(1);
        Employee employee = employeeService.getEmployeeByID(2);
        employeeService.addEmployee(employee);
        employeeService.updateEmployeeName("Test", 3);

        materialService.deleteMaterialById(1);
        Material material = materialService.getMaterialByID(2);
        materialService.addMaterial(material);
        materialService.updateMaterialName("Test", 3);

        providerService.deleteProviderById(1);
        Provider provider = providerService.getProviderByID(2);
        providerService.addProvider(provider);
        providerService.updateProviderName("Test", 3);

        stockService.deleteStockById(1);
        Stock stock = stockService.getStockByID(4);
        stockService.addStock(stock);
        stockService.updateStockName("Test", 3);
    }
}
