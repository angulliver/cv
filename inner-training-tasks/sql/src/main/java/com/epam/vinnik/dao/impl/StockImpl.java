package com.epam.vinnik.dao.impl;

import com.epam.vinnik.connectionTools.ConnectionManager;
import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.entity.Stock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StockImpl implements EntityDAO<Stock> {
    private Connection connection = ConnectionManager.getConnection();

    @Override
    public void updateName(String name, int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.STOCK_UPDATE_NAME)) {
            statement.setString(1, name);
            statement.setInt(2, id);

            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Stock getByID(int id) {
        Stock stock = new Stock();
        try (PreparedStatement statement = getByIdCreatePs(id, Queries.STOCK_GET_BY_ID);
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                stock.setSName(resultSet.getString("sName"));
                stock.setDepartmentId(resultSet.getInt("departmentID"));
            } else {
                throw new SQLException("Incorrect query.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return stock;
    }

    private PreparedStatement getByIdCreatePs(int id, String sqlQuery) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        statement.setInt(1, id);

        return statement;
    }

    @Override
    public void add(Stock obj) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.STOCK_ADD)) {
            statement.setString(1, obj.getSName());
            statement.setInt(2, obj.getDepartmentId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.STOCK_DELETE)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
