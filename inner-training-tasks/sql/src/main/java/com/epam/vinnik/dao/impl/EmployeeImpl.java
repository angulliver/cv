package com.epam.vinnik.dao.impl;

import com.epam.vinnik.connectionTools.ConnectionManager;
import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.entity.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeImpl implements EntityDAO<Employee> {
    private Connection connection = ConnectionManager.getConnection();

    @Override
    public void updateName(String name, int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.EMPLOYEE_UPDATE_NAME)) {
            statement.setString(1, name);
            statement.setInt(2, id);

            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Employee getByID(int id) {
        Employee employee = new Employee();
        try (PreparedStatement statement = getByIdCreatePs(id, Queries.EMPLOYEE_GET_BY_ID);
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                employee.setSurname(resultSet.getString("surname"));
                employee.setName(resultSet.getString("name"));
                employee.setSex(resultSet.getString("sex"));
                employee.setBirthday(resultSet.getDate("birthday").toLocalDate());
                employee.setMarried(resultSet.getBoolean("married"));
            } else {
                throw new SQLException("Incorrect query.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employee;
    }

    private PreparedStatement getByIdCreatePs(int id, String sqlQuery) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        statement.setInt(1, id);

        return statement;
    }

    @Override
    public void add(Employee obj) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.EMPLOYEE_ADD)) {
            statement.setString(1, obj.getSurname());
            statement.setString(2, obj.getName());
            statement.setString(3, obj.getSex());
            statement.setDate(4, java.sql.Date.valueOf(obj.getBirthday()));
            statement.setBoolean(5, obj.getMarried());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.EMPLOYEE_DELETE)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
