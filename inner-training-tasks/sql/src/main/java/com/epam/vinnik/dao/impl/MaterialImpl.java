package com.epam.vinnik.dao.impl;

import com.epam.vinnik.connectionTools.ConnectionManager;
import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.entity.Material;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MaterialImpl implements EntityDAO<Material> {
    private Connection connection = ConnectionManager.getConnection();

    @Override
    public void updateName(String name, int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.MATERIAL_UPDATE_NAME)) {
            statement.setString(1, name);
            statement.setInt(2, id);

            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Material getByID(int id) {
        Material material = new Material();
        try (PreparedStatement statement = getByIdCreatePs(id, Queries.MATERIAL_GET_BY_ID);
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                material.setMName(resultSet.getString("mName"));
                material.setPrice(resultSet.getInt("price"));
                material.setProviderId(resultSet.getInt("providerID"));
            } else {
                throw new SQLException("Incorrect query.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return material;
    }

    private PreparedStatement getByIdCreatePs(int id, String sqlQuery) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        statement.setInt(1, id);

        return statement;
    }

    @Override
    public void add(Material obj) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.MATERIAL_ADD)) {
            statement.setString(1, obj.getMName());
            statement.setInt(2, obj.getPrice());
            statement.setInt(3, obj.getProviderId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.MATERIAL_DELETE)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
