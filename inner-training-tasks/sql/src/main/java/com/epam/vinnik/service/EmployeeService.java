package com.epam.vinnik.service;

import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.dao.EntityDAOFactory;
import com.epam.vinnik.entity.Employee;

public class EmployeeService {
    private EntityDAO<Employee> employeeDAO;

    public EmployeeService() {
        this.employeeDAO = EntityDAOFactory.getDAO(Employee.class);
    }

    public void updateEmployeeName(String name, int id) {
        employeeDAO.updateName(name, id);
    }

    public Employee getEmployeeByID(int id) {
        return employeeDAO.getByID(id);
    }

    public void addEmployee(Employee obj) {
        employeeDAO.add(obj);
    }

    public void deleteEmployeeById(int id) {
        employeeDAO.deleteById(id);
    }
}
