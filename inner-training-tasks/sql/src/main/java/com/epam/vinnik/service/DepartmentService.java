package com.epam.vinnik.service;

import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.dao.EntityDAOFactory;
import com.epam.vinnik.entity.Department;

public class DepartmentService {
    private EntityDAO<Department> departmentDAO;

    public DepartmentService() {
        this.departmentDAO = EntityDAOFactory.getDAO(Department.class);
    }

    public void updateDepartmentName(String name, int id) {
        departmentDAO.updateName(name, id);
    }

    public Department getDepartmentByID(int id) {
        return departmentDAO.getByID(id);
    }

    public void addDepartment(Department obj) {
        departmentDAO.add(obj);
    }

    public void deleteDepartmentById(int id) {
        departmentDAO.deleteById(id);
    }
}
