package com.epam.vinnik.entity;


public class Stock {

    private int stockId;
    private String sName;
    private int departmentId;


    public int getStockId() {
        return stockId;
    }


    public String getSName() {
        return sName;
    }

    public void setSName(String sName) {
        this.sName = sName;
    }


    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

}
