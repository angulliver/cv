package com.epam.vinnik.dao.impl;

final class Queries {
    //DepartmentImpl
    static final String DEPARTMENT_UPDATE_NAME = "UPDATE sql_homework.departments SET dName = ? WHERE providerID = ?";
    static final String DEPARTMENT_GET_BY_ID = "SELECT * FROM sql_homework.departments WHERE departmentID = ?";
    static final String DEPARTMENT_ADD = "INSERT INTO sql_homework.departments (dName, materialID, providerID, employeeID) VALUES (?,?,?,?)";

    static final String DEPARTMENT_DELETE = "DELETE D,S FROM departments D INNER JOIN stocks S ON D.departmentID = S.departmentID WHERE D.departmentID = ?";

    //EmployeeImpl
    static final String EMPLOYEE_UPDATE_NAME = "UPDATE sql_homework.employees SET name = ? WHERE employeeID = ?";
    static final String EMPLOYEE_GET_BY_ID = "SELECT * FROM sql_homework.employees WHERE employeeID = ?";
    static final String EMPLOYEE_ADD = "INSERT INTO sql_homework.employees (surname, name, sex, birthday, married) VALUES (?,?,?,?,?)";
    static final String EMPLOYEE_DELETE = "DELETE FROM sql_homework.employees WHERE employeeID = ?";

    //MaterialImpl
    static final String MATERIAL_UPDATE_NAME = "UPDATE sql_homework.materials SET mName = ? WHERE materialID = ?";
    static final String MATERIAL_GET_BY_ID = "SELECT * FROM sql_homework.materials WHERE materialID = ?";
    static final String MATERIAL_ADD = "INSERT INTO sql_homework.materials (mName, price, providerID) VALUES (?,?,?)";
    static final String MATERIAL_DELETE = "DELETE FROM sql_homework.materials WHERE materialID = ?";

    //ProviderImpl
    static final String PROVIDER_UPDATE_NAME = "UPDATE sql_homework.providers SET pName = ? WHERE providerID = ?";
    static final String PROVIDER_GET_BY_ID = "SELECT * FROM sql_homework.providers WHERE providerID = ?";
    static final String PROVIDER_ADD = "INSERT INTO sql_homework.providers (pName, TIN, city) VALUES (?,?,?)";
    static final String PROVIDER_DELETE = "DELETE FROM sql_homework.providers WHERE providerID = ?";

    //StockImpl
    static final String STOCK_UPDATE_NAME = "UPDATE sql_homework.stocks SET sName = ? WHERE stockID = ?";
    static final String STOCK_GET_BY_ID = "SELECT * FROM sql_homework.stocks WHERE stockID = ?";
    static final String STOCK_ADD = "INSERT INTO sql_homework.stocks (sName, departmentID) VALUES (?,?)";
    static final String STOCK_DELETE = "DELETE FROM sql_homework.stocks WHERE stockID = ?";

    private Queries() {
    }
}
