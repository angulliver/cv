package com.epam.vinnik.dao.impl;

import com.epam.vinnik.connectionTools.ConnectionManager;
import com.epam.vinnik.dao.EntityDAO;
import com.epam.vinnik.entity.Department;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartmentImpl implements EntityDAO<Department> {
    private Connection connection = ConnectionManager.getConnection();

    @Override
    public void updateName(String name, int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.DEPARTMENT_UPDATE_NAME)) {
            statement.setString(1, name);
            statement.setInt(2, id);

            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Department getByID(int id) {
        Department department = new Department();
        try (PreparedStatement statement = getByIdCreatePs(id, Queries.DEPARTMENT_GET_BY_ID);
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                department.setDName(resultSet.getString("dName"));
                department.setMaterialId(resultSet.getInt("materialID"));
                department.setProviderId(resultSet.getInt("providerID"));
                department.setEmployeeId(resultSet.getInt("employeeID"));
            } else {
                throw new SQLException("Incorrect query.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return department;
    }

    private PreparedStatement getByIdCreatePs(int id, String sqlQuery) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        statement.setInt(1, id);

        return statement;
    }

    @Override
    public void add(Department obj) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.DEPARTMENT_ADD)) {
            statement.setString(1, obj.getDName());
            statement.setInt(2, obj.getMaterialId());
            statement.setInt(3, obj.getProviderId());
            statement.setInt(4, obj.getEmployeeId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.DEPARTMENT_DELETE)) {
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
