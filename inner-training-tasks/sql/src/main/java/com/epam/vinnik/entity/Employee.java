package com.epam.vinnik.entity;


import java.time.LocalDate;

public class Employee {

    private int employeeId;
    private String surname;
    private String name;
    private String sex;
    private LocalDate birthday;
    private boolean married;


    public int getEmployeeId() {
        return employeeId;
    }


    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }


    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }


    public boolean getMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

}
