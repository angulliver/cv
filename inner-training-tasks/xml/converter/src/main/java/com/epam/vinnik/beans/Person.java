package com.epam.vinnik.beans;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.Calendar;

public class Person implements Comparable<Person> {
    @XmlAttribute(name = "ID")
    private String personId;
    @XmlElement
    private String surname;
    @XmlElement
    private String name;
    @XmlElement
    private Birthday birthday;
    @XmlElement
    private Birthplace birthplace;
    @XmlElement
    private String work;

    public Person() {
    }

    public Person(String personId, String surname, String name, Birthday birthday, Birthplace birthplace, String work) {
        this.personId = personId;
        this.surname = surname;
        this.name = name;
        this.birthday = birthday;
        this.birthplace = birthplace;
        this.work = work;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public int getAge() {
        return Calendar.getInstance().get(Calendar.YEAR) - this.birthday.getYear();
    }

    @Override
    public int compareTo(Person o) {
        return o.getAge() - this.getAge();
    }

    @Override
    public String toString() {
        return "com.epam.vinnik.beans.Person{" +
                "personId='" + personId + '\'' +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", birthplace='" + birthplace + '\'' +
                ", work='" + work + '\'' +
                '}';
    }
}
