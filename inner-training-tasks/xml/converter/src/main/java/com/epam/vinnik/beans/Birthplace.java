package com.epam.vinnik.beans;

import javax.xml.bind.annotation.XmlAttribute;

public class Birthplace {
    @XmlAttribute
    private String city;

    public Birthplace() {
    }

    public Birthplace(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Birthplace{" +
                "city='" + city + '\'' +
                '}';
    }
}
