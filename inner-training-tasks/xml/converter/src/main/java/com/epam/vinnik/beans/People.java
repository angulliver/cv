package com.epam.vinnik.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@XmlRootElement
public class People {
    @XmlElement(name = "person")
    private List<Person> persons;

    public People() {
        this.persons = new ArrayList<>();
    }

    public void sortByAge() {
        this.persons.sort(Comparator.naturalOrder());
        updateIndexes();
    }

    private void updateIndexes() {
        int index = 0;
        for (Person person : persons) {
            person.setPersonId(String.valueOf(index));
            index++;
        }
    }

    @Override
    public String toString() {
        StringBuilder people = new StringBuilder();
        persons.forEach(person -> people.append(person.toString()));

        return people.toString();
    }
}
