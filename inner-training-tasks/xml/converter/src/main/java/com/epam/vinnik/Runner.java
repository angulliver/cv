package com.epam.vinnik;

import com.epam.vinnik.beans.People;
import com.epam.vinnik.parser.Parser;

import java.io.File;

public class Runner {

    public static void main(String[] args) throws Exception {
        File file = getFile();

        Parser parser = new Parser(getFile());
        parser.addNewUser(file);
        parser.editUser(file);
        People people = parser.unmarshalXml(file);
        people.sortByAge();
        parser.marshalXml(people, file);
    }

    public static File getFile() {
        ClassLoader classLoader = Runner.class.getClassLoader();
        return new File(classLoader.getResource("people.xml").getFile().replaceAll("%20", " "));
    }
}
