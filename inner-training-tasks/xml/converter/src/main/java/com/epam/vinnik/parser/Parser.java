package com.epam.vinnik.parser;

import com.epam.vinnik.beans.People;
import org.w3c.dom.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    private DocumentBuilderFactory documentBuilderFactory;
    private DocumentBuilder documentBuilder;
    private Document document;
    private Element root;

    private Parser() {
    }

    public Parser(File file) throws Exception {
        this.documentBuilderFactory = DocumentBuilderFactory.newInstance();
        this.documentBuilder = this.documentBuilderFactory.newDocumentBuilder();
        this.document = this.documentBuilder.parse(file);
        this.root = this.document.getDocumentElement();
    }

    public void addNewUser(File file) throws TransformerException {
        Element newPerson = document.createElement("person");

        Element surname = document.createElement("surname");
        surname.setTextContent("Vinnik");
        newPerson.appendChild(surname);

        Element name = document.createElement("name");
        name.setTextContent("Artsiom");
        newPerson.appendChild(name);

        Element birthday = document.createElement("birthday");

        Element day = document.createElement("day");
        day.setTextContent("14");

        Element month = document.createElement("month");
        month.setTextContent("04");

        Element year = document.createElement("year");
        year.setTextContent("1992");

        birthday.appendChild(day);
        birthday.appendChild(month);
        birthday.appendChild(year);
        newPerson.appendChild(birthday);

        Element birthplace = document.createElement("birthplace");
        birthplace.setAttribute("city", "Gomel");
        newPerson.appendChild(birthplace);

        Element work = document.createElement("work");
        work.setTextContent("Epam");
        newPerson.appendChild(work);

        Node person = document.getElementsByTagName("person").item(2);

        NamedNodeMap attributes = person.getAttributes();
        Node id = attributes.getNamedItem("ID");
        String index = id.getNodeValue();
        newPerson.setAttribute("ID", index);

        root.insertBefore(newPerson, person.getPreviousSibling());

        updateIndexes();
        writeToXml(file);
    }

    private void updateIndexes() {
        NodeList persons = document.getElementsByTagName("person");
        for (int i = 0; i < persons.getLength(); i++) {
            NamedNodeMap personAttr = persons.item(i).getAttributes();
            Node personId = personAttr.getNamedItem("ID");
            personId.setNodeValue(String.valueOf(i));
        }
    }

    public void editUser(File file) throws TransformerException {
        NodeList personsNames = document.getElementsByTagName("name");
        for (int i = 0; i < personsNames.getLength(); i++) {

            Node currPersonName = personsNames.item(i);
            String currPersonNameVal = currPersonName.getFirstChild().getTextContent();

            Pattern pattern = Pattern.compile("^(I)");
            Matcher matcher = pattern.matcher(currPersonNameVal);

            if (matcher.find()) {
                currPersonName.getFirstChild().setNodeValue(generateName());
            }
        }

        writeToXml(file);
    }

    public People unmarshalXml(File file) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(People.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        return (People) unmarshaller.unmarshal(file);
    }

    public void marshalXml(People people, File file) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(People.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(people, file);
        marshaller.marshal(people, System.out);
    }

    private String generateName() {
        String[] Beginning = {"Kr", "Ca", "Ra", "Mrok", "Cru",
                "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar", "Mjol",
                "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro",
                "Mar", "Luk"};
        String[] Middle = {"air", "ir", "mi", "sor", "mee", "clo",
                "red", "cra", "ark", "arc", "miri", "lori", "cres", "mur", "zer",
                "marac", "zoir", "slamar", "salmar", "urak"};
        String[] End = {"d", "ed", "ark", "arc", "es", "er", "der",
                "tron", "med", "ure", "zur", "cred", "mur"};

        Random rand = new Random();

        return Beginning[rand.nextInt(Beginning.length)] +
                Middle[rand.nextInt(Middle.length)] +
                End[rand.nextInt(End.length)];
    }

    private void writeToXml(File file) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "4");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(file);
        transformer.transform(source, result);
    }
}
