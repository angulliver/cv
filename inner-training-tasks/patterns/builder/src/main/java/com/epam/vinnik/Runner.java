package com.epam.vinnik;

public class Runner {
    public static void main(String[] args) {
        UserAccount user = new UserAccount.Builder("UserLogin", "UserPassword")
                .address("UserAddress")
                .name("UserName")
                .surname("UserSurname")
                .age(20)
                .build();

        System.out.println(user.toString());
    }
}
