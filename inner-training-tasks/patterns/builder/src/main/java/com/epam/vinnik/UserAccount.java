package com.epam.vinnik;

public class UserAccount {
    private final String name;
    private final String surname;
    private final String login;
    private final String password;
    private final String address;
    private final int age;

    public static class Builder {
        private final String login;
        private final String password;

        private String name = null;
        private String surname = null;
        private String address = null;
        private int age = 0;


        public Builder(String login, String password) {
            this.login = login;
            this.password = password;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder surname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public UserAccount build() {
            return new UserAccount(this);
        }
    }

    private UserAccount(Builder builder) {
        name = builder.name;
        surname = builder.surname;
        login = builder.login;
        password = builder.password;
        address = builder.address;
        age = builder.age;
    }

    @Override
    public String toString() {
        return "com.epam.vinnik.UserAccount{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                '}';
    }
}
