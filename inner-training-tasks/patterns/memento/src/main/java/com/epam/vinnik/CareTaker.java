package com.epam.vinnik;

public class CareTaker {

    private Memento memento;

    public void save(Originator originator){
        this.memento = originator.saveState();
    }

    public void undo(Originator originator){
        originator.undoToLastSaveState(this.memento);
    }
}
