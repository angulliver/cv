package com.epam.vinnik;

public class Originator {
    private String login;
    private String password;
    private int age;

    public Originator(String login, String password, int age) {
        this.login = login;
        this.password = password;
        this.age = age;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Memento saveState(){
        return new Memento(this.login,this.password,this.age);
    }

    public void undoToLastSaveState(Memento memento){
        this.login= memento.getLogin();
        this.password= memento.getPassword();
        this.age= memento.getAge();
    }

    @Override
    public String toString() {
        return "com.epam.vinnik.Originator{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                '}';
    }
}
