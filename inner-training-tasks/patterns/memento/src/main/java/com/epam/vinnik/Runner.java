package com.epam.vinnik;

public class Runner {
    public static void main(String[] args) {
        Originator originator = new Originator("UserLogin", "UserPassword", 22);

        System.out.println(originator);

        CareTaker careTaker = new CareTaker();
        careTaker.save(originator);

        originator.setAge(55);
        originator.setLogin("Java");

        System.out.println(originator.toString());

        careTaker.undo(originator);

        System.out.println(originator.toString());
    }
}
