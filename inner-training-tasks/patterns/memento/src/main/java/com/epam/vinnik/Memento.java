package com.epam.vinnik;

public class Memento {
    private String login;
    private String password;
    private int age;

    public Memento(String login, String password, int age) {
        this.login = login;
        this.password = password;
        this.age = age;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public int getAge() {
        return age;
    }
}
