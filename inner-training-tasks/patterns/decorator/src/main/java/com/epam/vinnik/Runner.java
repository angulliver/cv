package com.epam.vinnik;

public class Runner {
    public static void main(String[] args) {
        Drink tea = new Tea(20);
        Drink coffee = new Coffee(30);

        Drink teaWithMilk = new Milk(tea, 5);
        Drink teaWithMilkAndSugar = new Sugar(teaWithMilk, 2);

        Drink coffeeWithMilk = new Milk(coffee, 5);
        Drink coffeeWithMilkAndSugar = new Sugar(coffeeWithMilk, 2);

        System.out.println("Price of coffee: " + coffee.getPrice()
                +"\nPrice of coffee with milk: " + coffeeWithMilk.getPrice()
                +"\nPrice of coffee with milk and sugar: " + coffeeWithMilkAndSugar.getPrice()
                + "\nPrice of tea: " + tea.getPrice()
                +"\nPrice of tea with milk: " + teaWithMilk.getPrice()
                +"\nPrice of tea with milk and sugar: " + teaWithMilkAndSugar.getPrice());
    }
}
