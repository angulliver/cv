package com.epam.vinnik;

public class Tea implements Drink{
    private int price;

    public Tea(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
