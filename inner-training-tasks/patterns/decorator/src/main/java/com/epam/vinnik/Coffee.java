package com.epam.vinnik;

public class Coffee implements Drink {
    private int price;

    public Coffee(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
