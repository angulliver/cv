package com.epam.vinnik;

public interface Drink {
    int getPrice();
}
