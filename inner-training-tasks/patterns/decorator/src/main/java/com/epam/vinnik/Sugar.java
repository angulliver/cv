package com.epam.vinnik;

public class Sugar extends DrinkDecorator {
    private int price;

    protected Sugar(Drink drink, int price) {
        super(drink);
        this.price = price;
    }

    @Override
    public int getPrice() {
        return super.getPrice() + price;
    }
}
