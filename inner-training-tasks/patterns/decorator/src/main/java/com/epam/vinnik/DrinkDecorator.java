package com.epam.vinnik;

public abstract class DrinkDecorator implements Drink {
    protected final Drink drink;

    protected DrinkDecorator(Drink drink) {
        this.drink = drink;
    }

    public int getPrice() {
        return drink.getPrice();
    }
}
