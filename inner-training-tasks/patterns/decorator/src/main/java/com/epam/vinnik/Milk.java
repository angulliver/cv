package com.epam.vinnik;

public class Milk extends DrinkDecorator {
    private int price;

    protected Milk(Drink drink, int price) {
        super(drink);
        this.price = price;
    }

    @Override
    public int getPrice() {
        return super.getPrice() + price;
    }
}
