package com.epam.vinnik;

import com.epam.vinnik.consumers.Consumer1;
import com.epam.vinnik.consumers.Consumer2;
import com.epam.vinnik.producers.Producer;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class Runner {
    public static void main(String[] args) {
        LinkedBlockingQueue<Object> pool = new LinkedBlockingQueue<>();
        ReentrantLock locker = new ReentrantLock();

        new Thread(new Producer(pool, locker)).start();
        new Thread(new Producer(pool, locker)).start();
        new Thread(new Producer(pool, locker)).start();
        new Thread(new Producer(pool, locker)).start();

        new Thread(new Consumer1(pool, locker)).start();
        new Thread(new Consumer1(pool, locker)).start();
        new Thread(new Consumer1(pool, locker)).start();

        new Thread(new Consumer2(pool, locker)).start();
        new Thread(new Consumer2(pool, locker)).start();
        new Thread(new Consumer2(pool, locker)).start();
        new Thread(new Consumer2(pool, locker)).start();
        new Thread(new Consumer2(pool, locker)).start();
        new Thread(new Consumer2(pool, locker)).start();

    }
}
