package com.epam.vinnik.producers;

import com.epam.vinnik.tasks.TaskA;
import com.epam.vinnik.tasks.TaskB;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class Producer implements Runnable {
    private final LinkedBlockingQueue pool;
    private final ReentrantLock locker;

    public Producer(LinkedBlockingQueue pool, ReentrantLock locker) {
        this.pool = pool;
        this.locker = locker;
    }

    @Override
    public void run() {
        while (true) {
            locker.lock();
            try {
                pool.add(new TaskA());
                pool.add(new TaskB());
            } finally {
                locker.unlock();
            }
        }
    }
}
