package com.epam.vinnik.consumers;

import com.epam.vinnik.tasks.TaskB;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class Consumer2 implements Runnable {
    private final LinkedBlockingQueue pool;
    private final ReentrantLock locker;

    public Consumer2(LinkedBlockingQueue pool, ReentrantLock locker) {
        this.pool = pool;
        this.locker = locker;
    }

    @Override
    public void run() {
        while (true) {
            locker.lock();
            try {
                Object element = pool.peek();
                if (element instanceof TaskB) {
                    System.out.print("TaskB print ");
                    ((TaskB) element).print();
                    pool.remove();
                }
            } finally {
                locker.unlock();
            }
        }
    }
}
