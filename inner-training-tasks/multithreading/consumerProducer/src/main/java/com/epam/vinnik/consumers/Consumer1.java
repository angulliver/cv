package com.epam.vinnik.consumers;

import com.epam.vinnik.tasks.TaskA;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class Consumer1 implements Runnable {
    private final LinkedBlockingQueue pool;
    private final ReentrantLock locker;

    public Consumer1(LinkedBlockingQueue pool, ReentrantLock locker) {
        this.pool = pool;
        this.locker = locker;
    }

    @Override
    public void run() {
        while (true) {
            locker.lock();
            try {
                Object element = pool.peek();
                if (element instanceof TaskA) {
                    System.out.println("Task1 return " + ((TaskA) element).give());
                    pool.remove();
                }
            } finally {
                locker.unlock();
            }
        }
    }
}
