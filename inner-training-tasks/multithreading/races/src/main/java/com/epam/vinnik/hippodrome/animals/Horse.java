package com.epam.vinnik.hippodrome.animals;

public class Horse {
    private int horseNumber;
    private int horseSpeed;

    public Horse(int horseNumber, int horseSpeed) {
        this.horseNumber = horseNumber;
        this.horseSpeed = horseSpeed;
    }

    public int getHorseNumber() {
        return horseNumber;
    }

    public int getHorseSpeed() {
        return horseSpeed;
    }
}
