package com.epam.vinnik;

import com.epam.vinnik.hippodrome.Hippodrome;
import com.epam.vinnik.hippodrome.animals.Horse;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

public class Runner {

    private static int numberOfHorses;
    private static int distance;

    public static void main(String[] args) {
        CountDownLatch countDownLatch;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of horses:");
        numberOfHorses = scanner.nextInt();
        System.out.println("Enter the length of the route:");
        distance = scanner.nextInt();

        countDownLatch = new CountDownLatch(numberOfHorses);

        List<Horse> horses = new ArrayList<>();
        for (int i = 1; i <= numberOfHorses; i++) {
            horses.add(new Horse(i, (int) (Math.random() * 30 + 50)));
        }

        Hippodrome hippodrome = new Hippodrome(horses, distance, countDownLatch);

        hippodrome.start();

        while (countDownLatch.getCount() > 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
