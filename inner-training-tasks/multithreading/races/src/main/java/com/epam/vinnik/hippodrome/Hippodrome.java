package com.epam.vinnik.hippodrome;

import com.epam.vinnik.hippodrome.animals.Horse;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;

public class Hippodrome {
    private List<Horse> horses;
    private int distance;
    private CountDownLatch countDownLatch;

    public Hippodrome(List<Horse> horses, int distance, CountDownLatch countDownLatch) {
        this.horses = horses;
        this.distance = distance;
        this.countDownLatch = countDownLatch;
    }

    public void start() {
        for (Horse horse : horses) {
            new Thread(new Move(horse, countDownLatch)).start();
        }
    }

    private class Move implements Runnable {
        Horse horse;
        CountDownLatch countDownLatch;

        private final Logger LOGGER = Logger.getLogger(Move.class.getName());

        public Move(Horse horse, CountDownLatch countDownLatch) {
            this.countDownLatch = countDownLatch;
            this.horse = horse;
        }

        @Override
        public void run() {
            try {
                countDownLatch.countDown();
                countDownLatch.await();

                int residualDistance = distance;

                while (true) {
                    Thread.sleep(1000);

                    residualDistance = residualDistance - horse.getHorseSpeed();

                    if (residualDistance >= 0) {
                        LOGGER.info("Horse number " + horse.getHorseNumber() + " left " + residualDistance);
                    } else {
                        LOGGER.info("Horse number " + horse.getHorseNumber() + " finished");
                        break;
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
