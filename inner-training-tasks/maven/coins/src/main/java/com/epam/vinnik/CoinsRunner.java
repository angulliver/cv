package com.epam.vinnik;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CoinsRunner {

    private final static int numOfParameters = 2;

    public static void main(String[] args) {

        if (args.length != numOfParameters) {
            System.err.println("Отсутствуют параметры!");
            System.exit(0);
        }

        String inputFilePath = args[0];
        String outputFilePath = args[1];

        List<String> readedFile = readFile(inputFilePath);

        Coin coin = new Coin();
        boolean result = new File(outputFilePath).delete();

        for (String line : readedFile) {
            try{
                List<List<Integer>> currentResult = coin.getValues(line);
                writeFile(currentResult, outputFilePath);
            } catch (CoinException e){
                writeErrToFile(e.getMessage(), outputFilePath);
            }
        }
    }

    private static List<String> readFile(String inputFilePath) {
        List<String> list = null;
        try (Stream<String> lines = Files.lines(Paths.get(inputFilePath))) {
            list = lines.collect(Collectors.toList());
        } catch (IOException e) {
            System.err.println("Отсутствует входной файл!");
            System.exit(0);
        }
        return list;
    }

    private static void writeFile(List<List<Integer>> currentResult, String outputFilePath) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(outputFilePath, true))) {

            for (List<Integer> lines : currentResult) {
                for (Integer s : lines) {
                    writer.write(s + " ");
                }
                writer.write("\n");
            }
            writer.write("\n");
        } catch (IOException e) {
            new File(outputFilePath);
        }
    }

    private static void writeErrToFile(String error, String outputFilePath) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(outputFilePath, true))) {
            writer.write(error + "\n");
            writer.write("\n");
        } catch (IOException e) {
            new File(outputFilePath);
        }
    }
}
