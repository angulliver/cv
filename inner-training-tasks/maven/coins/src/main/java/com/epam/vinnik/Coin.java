package com.epam.vinnik;

import java.util.*;

public class Coin {

    private static void getResult(List<List<Integer>> result, List<Integer> cur, int[] candidates, int target, int start) {
        if (target > 0) {
            for (int i = start; i < candidates.length && target >= candidates[i]; i++) {
                cur.add(candidates[i]);
                getResult(result, cur, candidates, target - candidates[i], i);
                cur.remove(cur.size() - 1);
            }
        } else if (target == 0) {
            result.add(new ArrayList<>(cur));
        }
    }

    private List<List<Integer>> combinationSum(int[] candidates, int target) throws CoinException {
        Arrays.sort(candidates);

        List<List<Integer>> result = new ArrayList<>();
        getResult(result, new ArrayList<>(), candidates, target, 0);

        if (result.size() == 0) {
            throw new CoinException("Неверное выражение: Нельзя разбить.");
        }
        return result;
    }

    public List<List<Integer>> getValues (String line) throws CoinException {
        try {
            int[] numbers = Arrays.stream(line.split("\\s+"))
                    .mapToInt(Integer::parseInt)
                    .toArray();

            int target = numbers[0];
            int[] candidates = Arrays.copyOfRange(numbers, 1, numbers.length);

            checkExpression(candidates, target);

            return combinationSum(candidates, target);

        } catch (NumberFormatException e) {
            throw new CoinException("Неверное выражение:  Ошибка с преобразованием в число.");
        }
    }

    private void checkExpression(int[] candidates, int target) throws CoinException {
        Set<Integer> set = new HashSet<>();
        for (int candidate : candidates) {
            if (!set.add(candidate)) {
                throw new CoinException("Неверное выражение: Номиналы повторяются.");
            }
        }

        if (target < 1 || candidates.length == 0) {
            throw new CoinException("Неверное выражение: Нет номиналов.");
        }
    }
}
