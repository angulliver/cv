package com.epam.vinnik;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    private String operators = "+-*/";
    private String delimiters = "()." + operators;

    private boolean isOperator(String token) {
        if (token.length() != 1) return false;
        for (int i = 0; i < operators.length(); i++) {
            if (token.charAt(0) == operators.charAt(i)) return true;
        }
        return false;
    }

    private boolean isDelimiter(String token) {
        if (token.length() != 1) return false;
        for (int i = 0; i < delimiters.length(); i++) {
            if (token.charAt(0) == delimiters.charAt(i)) return true;
        }
        return false;
    }

    private boolean isUnaryMinus(String token) {
        if (token.equals("u-")) return true;
        for (int i = 0; i < operators.length(); i++) {
            if (token.charAt(0) == operators.charAt(i)) return true;
        }
        return false;
    }

    private int priority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    private void checkInfixLine(String infix) throws CalculatorException {
        Pattern pattern = Pattern.compile("-[(]\\d+[)]");
        Matcher matcher = pattern.matcher(infix);

        if (matcher.find()) {
            throw new CalculatorException("Неверное выражение: Арифметическая ошибка.");
        }
    }

    private List<String> parseToRPN(String infix) throws CalculatorException {

        infix = infix.replaceAll("\\s+", "");
        checkInfixLine(infix);
        List<String> postfix = new ArrayList<>();
        Deque<String> stack = new ArrayDeque<>();
        StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);
        String prev = "";
        String curr = "";
        while (tokenizer.hasMoreTokens()) {
            curr = tokenizer.nextToken();
            if ((!tokenizer.hasMoreTokens() && isUnaryMinus(curr))
                    || (isOperator(curr) && isOperator(prev))
                    || curr.equals(".")) {
                throw new CalculatorException("Неверное выражение: Арифметическая ошибка.");
            }
            if (isDelimiter(curr)) {
                if (curr.equals("(")) stack.push(curr);
                else if (curr.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        postfix.add(stack.pop());
                        if (stack.isEmpty()) {
                            throw new CalculatorException("Неверное выражение: Скобочки.");
                        }
                    }
                    stack.pop();
                    if (!stack.isEmpty()) {
                        postfix.add(stack.pop());
                    }
                } else {
                    if (curr.equals("-") && (prev.equals("") || (isDelimiter(prev) && !prev.equals(")")))) {
                        curr = "u-";
                    }
                    else {
                        while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                            postfix.add(stack.pop());
                        }
                    }
                    stack.push(curr);
                }

            } else {
                postfix.add(curr);
            }
            prev = curr;
        }

        while (!stack.isEmpty()) {
            if (isUnaryMinus(stack.peek())) postfix.add(stack.pop());
            else {
                throw new CalculatorException("Неверное выражение: Скобочки.");
            }
        }
        return postfix;
    }

    public String calculate(String infix) throws CalculatorException {

        List<String> postfix = parseToRPN(infix);
        Deque<Double> stack = new ArrayDeque<>();

        for (String x : postfix) {
            switch (x) {
                case "+":
                    stack.push(stack.pop() + stack.pop());
                    break;
                case "-": {
                    Double b = stack.pop(), a = stack.pop();
                    stack.push(a - b);
                    break;
                }
                case "*":
                    stack.push(stack.pop() * stack.pop());
                    break;
                case "/": {
                    Double b = stack.pop(), a = stack.pop();
                    stack.push(a / b);
                    break;
                }
                case "u-":
                    stack.push(-stack.pop());
                    break;
                default:
                    stack.push(Double.valueOf(x));
                    break;
            }
        }
        return String.valueOf(stack.pop());
    }
}
