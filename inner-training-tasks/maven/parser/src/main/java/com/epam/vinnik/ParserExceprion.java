package com.epam.vinnik;

public class ParserExceprion extends Exception {
    public ParserExceprion() {
        super();
    }

    public ParserExceprion(String message) {
        super(message);
    }

    public ParserExceprion(String message, Throwable cause) {
        super(message, cause);
    }

    public ParserExceprion(Throwable cause) {
        super(cause);
    }

    protected ParserExceprion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
