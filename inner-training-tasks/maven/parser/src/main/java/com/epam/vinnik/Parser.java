package com.epam.vinnik;

import com.google.gson.*;

import java.util.Iterator;
import java.util.Set;

public class Parser {
    public String parse(String jsonLine) throws JsonSyntaxException {
        StringBuilder result = new StringBuilder();

        JsonElement jsonElement = new JsonParser().parse(jsonLine);
        JsonObject  jsonObject = jsonElement.getAsJsonObject();

        Set<String> keys = jsonObject.keySet();
        for (String key : keys){
            result.append(key).append(" ")
                    .append(jsonObject.get(key).getAsString()).append(" ");
        }
        return result.toString();
    }
}
