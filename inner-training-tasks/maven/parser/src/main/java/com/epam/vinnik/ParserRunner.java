package com.epam.vinnik;

import com.google.gson.JsonSyntaxException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParserRunner {
    private final static int numOfParameters = 2;

    public static void main(String[] args) {

        if (args.length != numOfParameters){
            System.err.println("Отсутствуют параметры!");
            System.exit(0);
        }

        String inputFilePath = args[0];
        String outputFilePath = args[1];

        List<String> readedFile =  readFile(inputFilePath);

        Parser parser = new Parser();

        for (int i = 0;  i < readedFile.size(); i++){

            try {
                String currentLine = parser.parse(readedFile.get(i));
                readedFile.set(i, currentLine.trim()) ;
            } catch (JsonSyntaxException e){
                readedFile.set(i, "Неверный JSON объект!") ;
            }

        }

        writeFile(readedFile, outputFilePath);
    }

    private static List<String> readFile (String inputFilePath){
        List<String> list = null;
        try (Stream<String> lines = Files.lines(Paths.get(inputFilePath))) {
            list = lines.collect(Collectors.toList());
        } catch (IOException e) {
            System.err.println("Отсутствует входной файл!");
        }
        return list;
    }

    private static void writeFile(List<String> lines, String outputFilePath){
        while (true){
            Path file = Paths.get(outputFilePath);
            try {
                Files.write(file, lines, StandardCharsets.UTF_8);
                break;
            } catch (IOException e) {
                new File(outputFilePath);
            }
        }
    }
}
