package com.epam.vinnik;

import java.util.EnumSet;

public class Decoder {

    public String getTranscript (String abbreviation){
        String transcript = "Неверная аббревиатура!";

        EnumSet<Abbreviation> abbreviations = EnumSet.allOf(Abbreviation.class);
        for (Abbreviation abb : abbreviations){
            if(abbreviation.equals(abb.name())){
                transcript =abb.getTranscript();
            }
        }
        return transcript;
    }
}
