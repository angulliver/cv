package com.epam.vinnik;

public enum Abbreviation {
    АСУ ("автоматизированная система управления"),
    ВУЗ ("высшее учебное заведение"),
    ГЭС ("гидроэлектростанция"),
    ОДУ ("обыкновенное дифференциальное уравнение"),
    ТАСС ("телеграфное агентство Советского Союза"),
    ЯОД ("язык описания данных");

    Abbreviation(String transcript) {
        this.transcript = transcript;
    }

    private  String transcript;

    public String getTranscript() {
        return transcript;
    }
}
